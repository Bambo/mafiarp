-- This is only called server anyway
local oldCount = _R.Player.AddCount
local oldClean = cleanup.Add
local function propOwn(ply,type,ent)
	ent.Owner = ply
	local recip = RecipientFilter()
	recip:AddAllPlayers()
	recip:RemovePlayer(ply)
	umsg.Start("PropOwn", recip)
		umsg.Entity(ent)
		umsg.Entity(ply)
	umsg.End()
end

function _R.Player.AddCount(ply,type,ent) propOwn(ply,type,ent) oldCount(ply,type,ent) end
function cleanup.Add(ply,type,ent) propOwn(ply,type,ent) oldClean(ply,type,ent) end

usermessage.Hook("PropOwn", function(um)
	um:ReadEntity().Owner = um:ReadEntity()	-- nice
end)

hook.Add("GravGunPickupAllowed", "PropGrav", function(ply, ent)
	return !!ent.Owner
end)

hook.Add("GravGunPunt", "PropPunt", function(ply, ent)
	if SERVER then DropEntityIfHeld(ent) end
	return false
end)

-- dunno?
hook.Add("PlayerUse", "PropUse", function(ply, ent)
end)

hook.Add("OnPhysgunReload", function(wep,ply)
	local ent = ply:GetEyeTrace().Entity
	if not ValidEntity(ent) then return end
	if ent.Owner == ply then return true else return false end
end)

hook.Add("PhysgunPickup", "PropPickup", function(ply, ent)
	if !ent.untouchable and (ent.Owner == ply or (ply:IsAdmin() and ent.Owner) or (ply:IsAdmin() and ent:IsPlayer())) then
		ent.oldCollision = ent:GetCollisionGroup()
		ent:SetCollisionGroup(COLLISION_GROUP_WEAPON)
		return true
	else return false end
end)

hook.Add("PhysgunDrop", "PropDrop", function(ply,ent)
	if ent.oldCollision then ent:SetCollisionGroup(ent.oldCollision) end
end)

hook.Add("CanTool", "PropTool", function(ply, trace, tool)
	if trace.Entity.untouchable then return false end
	-- maybe we should put these in a table?
	if tool == "duplicator" or tool == "rope" or tool == "ignite" or tool == "dynamite" or tool == "thruster" or tool == "paint" or tool == "wheel" or tool == "emitter" or tool == "hoverball" or tool == "slider" or tool == "rope" or tool == "hydraulic" or tool == "nail" or tool == "pulley" or tool == "elastic" or tool == "winch" or tool == "trails" then return false end

	if ply:IsAdmin() and trace.Entity.Owner then return true end

	if trace.Entity.CanTool then
		return trace.Entity:CanTool(ply, trace, tool)
	end

	if (trace.Entity.Owner == ply or trace.Entity:IsWorld()) then
		return true
	else return false end
end)

hook.Add("PlayerDisconnected", "PropDisconnect", function(ply)
	for k,v in pairs(ents.GetAll()) do -- MAYBE WE SHOULD DO SOMETHING ABOUT THE MONEY PRINTER GLITCH?
		if v.Owner == ply then v:Remove() end
	end
end)
