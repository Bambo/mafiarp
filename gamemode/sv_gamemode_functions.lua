/*---------------------------------------------------------
 Gamemode functions
 ---------------------------------------------------------*/
-- Grammar corrections by Eusion
function GM:Initialize()
	self.BaseClass:Initialize()
end

function GM:PlayerSpawnProp(ply, model)
	if not self.BaseClass:PlayerSpawnProp(ply, model) then return false end

	local allowed = false

	if RPArrestedPlayers[ply:SteamID()] then return false end
	model = string.gsub(model, "\\", "/")
	if string.find(model,  "//") then Notify(ply, 1, 4, "You can't spawn this prop as it contains an invalid path. " ..model) 
	DB.Log(ply:SteamName().." ("..ply:SteamID()..") tried to spawn prop with an invalid path "..model) return false end
	-- Banned props take precedence over allowed props
	if CfgVars["banprops"] == 1 then
		for k, v in pairs(BannedProps) do
			if string.lower(v) == string.lower(model) then 
				Notify(ply, 1, 4, "You can't spawn this prop as it is banned. "..model) 
				DB.Log(ply:SteamName().." ("..ply:SteamID()..") tried to spawn banned prop "..model)
				return false 
			end
		end
	end

	-- If prop spawning is enabled or the user has admin or prop privileges
	if CfgVars["propspawning"] == 1 or ply:HasPriv(ADMIN) or ply:HasPriv(PROP) then
		-- If we are specifically allowing certain props, if it's not in the list, allowed will remain false
		if CfgVars["allowedprops"] == 1 then
			for k, v in pairs(AllowedProps) do
				if v == model then allowed = true end
			end
		else
			-- allowedprops is not enabled, so assume that if it wasn't banned above, it's allowed
			allowed = true
		end
	end

	if allowed then
		if CfgVars["proppaying"] == 1 then
			if ply:CanAfford(CfgVars["propcost"]) then
				Notify(ply, 1, 4, "Deducted " .. CUR .. CfgVars["propcost"])
				ply:AddMoney(-CfgVars["propcost"])
				return true
			else
				Notify(ply, 1, 4, "Need " .. CUR .. CfgVars["propcost"])
				return false
			end
		else
			return true
		end
	end
	return false
end

function GM:PlayerSpawnSENT(ply, model)
	return self.BaseClass:PlayerSpawnSENT(ply, model) and not RPArrestedPlayers[ply:SteamID()]
end

function GM:PlayerSpawnSWEP(ply, model)
	return self.BaseClass:PlayerSpawnSWEP(ply, model) and not RPArrestedPlayers[ply:SteamID()]
end

function GM:PlayerSpawnEffect(ply, model)
	return self.BaseClass:PlayerSpawnEffect(ply, model) and not RPArrestedPlayers[ply:SteamID()]
end

function GM:PlayerSpawnVehicle(ply, model)
	return self.BaseClass:PlayerSpawnVehicle(ply, model) and not RPArrestedPlayers[ply:SteamID()]
end

function GM:PlayerSpawnNPC(ply, model)
	return self.BaseClass:PlayerSpawnNPC(ply, model) and not RPArrestedPlayers[ply:SteamID()]
end

function GM:PlayerSpawnRagdoll(ply, model)
	return self.BaseClass:PlayerSpawnRagdoll(ply, model) and not RPArrestedPlayers[ply:SteamID()]
end

function GM:PlayerSpawnedProp(ply, model, ent)
	self.BaseClass:PlayerSpawnedProp(ply, model, ent)
	ent.SID = ply.SID
	ent.Owner = ply
end

function GM:PlayerSpawnedRagdoll(ply, model, ent)
	self.BaseClass:PlayerSpawnedRagdoll(ply, model, ent)
	ent.SID = ply.SID
end

function GM:EntityRemoved(ent)
	self.BaseClass:EntityRemoved(ent)
	if ent:IsVehicle() then
		local found = ent.Owner
		if ValidEntity(found) then
			found.Vehicles = found.Vehicles or 1
			found.Vehicles = found.Vehicles - 1
		end
	end
	
	for k,v in pairs(DarkRPEntities) do
		if ent:IsValid() and ent:GetClass() == v.ent and ValidEntity(ent.Owner) and not ent.IsRemoved then
			local ply = ent.Owner
			local cmdname = string.gsub(v.ent, " ", "_")
			if not ply["max"..cmdname] then
				ply["max"..cmdname] = 1
			end
			ply["max"..cmdname] = ply["max"..cmdname] - 1
			ent.IsRemoved = true
		end
	end
end

function GM:ShowSpare1(ply)
	umsg.Start("ToggleClicker", ply)
	umsg.End()
end

function GM:ShowSpare2(ply)
	ply:SendLua("ChangeJobVGUI()")
end

local materials_drop = {}
materials_drop["npc_zombie"] = { { "material_wire", "models/props_c17/TrapPropeller_Lever.mdl" } }

function GM:OnNPCKilled(victim, ent, weapon)
	-- If something killed the npc
	if ent then
		if ent:IsVehicle() and ent:GetDriver():IsPlayer() then ent = ent:GetDriver() end

		-- if it wasn't a player directly, find out who owns the prop that did the killing
		if not ent:IsPlayer() then
			ent = FindPlayerBySID(ent.SID)
		end
		local does = math.random( 1, 10 )
		if does >= 6 then
			if materials_drop[victim:GetClass()] ~= nil then
				local rand = math.random( 1, #materials_drop[victim:GetClass()] )
				local model = materials_drop[victim:GetClass()][rand][2]
				invAdd( ent, rand, model )
				Notify( ent, 3, 2, "You just aquired wire!")
			else
				local rand = math.random( 1, #materials_drop["npc_zombie"] )
				local model = materials_drop["npc_zombie"][rand][2]
				invAdd( ent, rand, model )
				Notify( ent, 3, 2, "You just aquired wire!")
			end
		end
	end
end

function GM:KeyPress(ply, code)
	self.BaseClass:KeyPress(ply, code)

	if code == IN_USE then
		local trace = { }
		trace.start = ply:EyePos()
		trace.endpos = trace.start + ply:GetAimVector() * 95
		trace.filter = ply
		local tr = util.TraceLine(trace)

		if ValidEntity(tr.Entity) and not ply:KeyDown(IN_ATTACK) then
			if tr.Entity:GetTable().Letter then
				umsg.Start("ShowLetter", ply)
					umsg.Short(tr.Entity.type)
					umsg.Vector(tr.Entity:GetPos())
					local numParts = tr.Entity.numPts
					umsg.Short(numParts)
					for a,b in pairs(tr.Entity.Parts) do umsg.String(b) end
				umsg.End()
			end
		else
			umsg.Start("KillLetter", ply)
			umsg.End()
		end
	end
end

function GM:PlayerCanHearPlayersVoice(listener, talker)
	if ValidEntity(listener:GetNWEntity("phone")) and ValidEntity(talker:GetNWEntity("phone")) and listener == talker:GetNWEntity("phone").Caller then 
		return true
	elseif ValidEntity(talker:GetNWEntity("phone")) then
		return false
	end
	
	if CfgVars["voiceradius"] == 1 and listener:GetShootPos():Distance(talker:GetShootPos()) < 550 then
		return true
	elseif CfgVars["voiceradius"] == 1 then
		return false
	end
	return true
end

function GM:CanTool(ply, trace, mode)
	if not self.BaseClass:CanTool(ply, trace, mode) then return false end

	if ValidEntity(trace.Entity) then
		if trace.Entity.onlyremover then
			if mode == "remover" then
				return (ply:IsAdmin() or ply:IsSuperAdmin())
			else
				return false
			end
		end

		/*
		if trace.Entity.nodupe and (mode == "weld" or
					mode == "weld_ez" or
					mode == "spawner" or
					mode == "duplicator" or
					mode == "adv_duplicator") then
			return false
		end
		*/
		
		if trace.Entity:IsVehicle() and mode == "nocollide" and CfgVars["allowvnocollide"] == 0 then
			return false
		end
	end
	return true
end

function GM:CanPlayerSuicide(ply)
	if ply.IsSleeping then
		Notify(ply, 4, 4, string.format(LANGUAGE.unable, "suicide"))
		return false
	end
	if RPArrestedPlayers[ply:SteamID()] then
		Notify(ply, 4, 4, string.format(LANGUAGE.unable, "suicide"))
		return false
	end
	return true
end

function GM:DoPlayerDeath(ply, attacker, dmginfo, ...)
	if tobool(CfgVars["dropweapondeath"]) and ValidEntity(ply:GetActiveWeapon()) then
		ply:DropWeapon(ply:GetActiveWeapon())
	end
	ply:CreateRagdoll()
	ply:AddDeaths( 1 )
	ply:Extinguish()
	if ValidEntity(attacker) and attacker:IsPlayer() then
		if attacker == ply then
			attacker:AddFrags( -1 )
		else
			attacker:AddFrags( 1 )
		end
	end
end

function GM:PlayerDeath(ply, weapon, killer)
	if GetGlobalInt("deathblack") == 1 then
		local RP = RecipientFilter()
		RP:RemoveAllPlayers()
		RP:AddPlayer(ply)
		umsg.Start("DarkRPEffects", RP)
			umsg.String("colormod")
			umsg.String("1")
		umsg.End()
		RP:AddAllPlayers()
	end
	UnDrugPlayer(ply)

	if weapon:IsVehicle() and weapon:GetDriver():IsPlayer() then killer = weapon:GetDriver() end
	if GetGlobalInt("deathnotice") == 1 then
		self.BaseClass:PlayerDeath(ply, weapon, killer)
	end

	ply:Extinguish()

	if ply:InVehicle() then ply:ExitVehicle() end

	if RPArrestedPlayers[ply:SteamID()] then
		-- If the player died in jail, make sure they can't respawn until their jail sentance is over
		ply.NextSpawnTime = CurTime() + math.ceil(GetGlobalInt("jailtimer") - (CurTime() - ply.LastJailed)) + 1
		for a, b in pairs(player.GetAll()) do
			b:PrintMessage(HUD_PRINTCENTER, string.format(LANGUAGE.died_in_jail, ply:Nick()))
		end 
		Notify(ply, 4, 4, LANGUAGE.dead_in_jail)
	else
		-- Normal death, respawning.
		ply.NextSpawnTime = CurTime() + CfgVars["respawntime"]
	end
	ply:GetTable().DeathPos = ply:GetPos()
	
	if CfgVars["dmautokick"] == 1 and killer and killer:IsPlayer() and killer ~= ply then
		if not killer.kills or killer.kills == 0 then
			killer.kills = 1
			timer.Simple(CfgVars["dmgracetime"], killer.ResetDMCounter, killer)
		else
			-- if this player is going over their limit, kick their ass
			if killer.kills + 1 > CfgVars["dmmaxkills"] then
				game.ConsoleCommand("kickid " .. killer:UserID() .. " Auto-kicked. Excessive Deathmatching.\n")
			else
				-- killed another player
				killer.kills = killer.kills + 1
			end
		end
	end

	if ValidEntity(ply) and ply ~= killer or ply:GetTable().Slayed then
		ply:SetNWBool("wanted", false)
		ply:GetTable().DeathPos = nil
		ply:GetTable().Slayed = false
	end
	
	ply:GetTable().ConfisquatedWeapons = nil
	if weapon:IsPlayer() then weapon = weapon:GetActiveWeapon() killer = killer:SteamName() if ( !weapon || weapon == NULL ) then weapon = killer else weapon = weapon:GetClass() end end
	if killer == ply then killer = "Himself" weapon = "suicide trick" end
	DB.Log(ply:Nick() .. " was killed by "..tostring(killer) .. " with a "..tostring(weapon))
end

local weplevels = {}			// Table to hold the levels of the items
weplevels["weapon_glock2"] = 1
weplevels["weapon_deagle2"] = 5
weplevels["weapon_p2282"] = 10
weplevels["weapon_fiveseven2"] = 7

weplevels["weapon_tmp"] = 30
weplevels["weapon_mac102"] = 20
weplevels["weapon_mp52"] = 25
weplevels["weapon_m42"] = 30
weplevels["weapon_ak472"] = 35
weplevels["weapon_pumpshotgun2"] = 45
weplevels["weapon_para"] = 50

weplevels["arrest_stick"] = 16
weplevels["unarrest_stick"] = 16
weplevels["door_ram"] = 16
weplevels["med_kit"] = 10
weplevels["stunstick"] = 16
weplevels["weaponchecker"] = 16

weplevels["weapon_physgun"] = 1
weplevels["weapon_physcannon"] = 1
weplevels["gmod_tool"] = 1
weplevels["gmod_camera"] = 1
weplevels["keys"] = 1
weplevels["backpack"] = 1

weplevels["theif_bag"] = 15
weplevels["lockpick"] = 15
weplevels["keypad_cracker"] = 15
weplevels["master_lockpick"] = 30
weplevels["master_keypad"] = 30

function GM:PlayerCanPickupWeapon(ply, weapon)
	if (type(players_table[ply:SteamID()]) != "table") then
		return false
	else
		local level = tonumber(GetLevel( ply ))
		if weplevels[weapon:GetClass()] == nil or level == nil then return true end
		if level >= weplevels[weapon:GetClass()] then
			if RPArrestedPlayers[ply:SteamID()] then ply:ChatPrint("You are arrested") return false end
			return true
		else
			return false
		end
	end
end

local function IsEmpty(vector)
	local point = util.PointContents(vector)
	local a = point ~= CONTENTS_SOLID 
	and point ~= CONTENTS_MOVEABLE 
	and point ~= CONTENTS_LADDER 
	and point ~= CONTENTS_PLAYERCLIP 
	and point ~= CONTENTS_MONSTERCLIP
	local b = true
	
	for k,v in pairs(ents.FindInSphere(vector, 35)) do
		if v:IsNPC() or v:IsPlayer() or v:GetClass() == "prop_physics" then
			b = false
		end
	end
	return a and b
end

local function removelicense(ply) 
	if not ValidEntity(ply) then return end 
	ply:GetTable().RPLicenseSpawn = false 
end

local CivModels = {
	"models/player/group01/male_01.mdl",
	"models/player/Group01/Male_02.mdl",
	"models/player/Group01/male_03.mdl",
	"models/player/Group01/Male_04.mdl",
	"models/player/Group01/Male_05.mdl",
	"models/player/Group01/Male_06.mdl",
	"models/player/Group01/Male_07.mdl",
	"models/player/Group01/Male_08.mdl",
	"models/player/Group01/Male_09.mdl"
}
function GM:PlayerSetModel(ply)
	local EndModel = ""
	if CfgVars["enforceplayermodel"] == 1 then
		for k,v in pairs(RPExtraTeams) do
			if ply:Team() == (k) then
				EndModel = v.model
			end
		end
		
		if ply:Team() == TEAM_CITIZEN then
			local validmodel = false
			for k, v in pairs(CivModels) do
				if ply:GetTable().PlayerModel == v then
					validmodel = true
					break
				end
			end
			
			if not validmodel then
				ply:GetTable().PlayerModel = nil
			end
			
			local model = ply:GetModel()
			
			if model ~= ply:GetTable().PlayerModel then
				for k, v in pairs(CivModels) do
					if v == model then
						ply:GetTable().PlayerModel = model
						validmodel = true
						break
					end
				end
				
				if not validmodel and not ply:GetTable().PlayerModel then
					ply:GetTable().PlayerModel = CivModels[math.random(1, #CivModels)]
				end
				
				EndModel = ply:GetTable().PlayerModel
			end
		else
			local cl_playermodel = ply:GetInfo( "cl_playermodel" )
			local EndModel = player_manager.TranslatePlayerModel( cl_playermodel )
		end
		util.PrecacheModel(EndModel)
		ply:SetModel(EndModel)
	else
		local cl_playermodel = ply:GetInfo( "cl_playermodel" )
        local modelname = player_manager.TranslatePlayerModel( cl_playermodel )
        util.PrecacheModel( modelname )
        ply:SetModel( modelname )
	end
end

function GM:PlayerInitialSpawn(ply)
	self.BaseClass:PlayerInitialSpawn(ply)
	DB.Log(ply:SteamName().." ("..ply:SteamID()..") has joined the game")
	ply.bannedfrom = {}
	ply:NewData()
	ply.SID = ply:UserID()
	DB.RetrieveSalary(ply)
	DB.RetrieveMoney(ply)
	timer.Simple(10, ply.CompleteSentence, ply)
end

function GM:PlayerSelectSpawn(ply)
	local POS = self.BaseClass:PlayerSelectSpawn(ply)
	if POS.GetPos then 
		POS = POS:GetPos()
	else
		POS = ply:GetPos()
	end 
	
	
	local CustomSpawnPos = DB.RetrieveTeamSpawnPos(ply)
	if CfgVars["customspawns"] == 1 and not RPArrestedPlayers[ply:SteamID()] and CustomSpawnPos then
		POS = CustomSpawnPos[math.random(1, #CustomSpawnPos)]
	end
	
	-- Spawn where died in certain cases
	if (CfgVars["strictsuicide"] == 1 or RPArrestedPlayers[ply:SteamID()]) and ply:GetTable().DeathPos then
		if not (RPArrestedPlayers[ply:SteamID()]) then
			POS = ply:GetTable().DeathPos
		end
	end
	
	if not IsEmpty(POS) then
		local found = false
		for i = 40, 300, 15 do
			if IsEmpty(POS + Vector(i, 0, 0)) then
				POS = POS + Vector(i, 0, 0)
				--Yeah I found a nice position to put the player in!
				found = true
				break
			end
		end
		if not found then
			for i = 40, 300, 15 do
				if IsEmpty(POS + Vector(0, i, 0)) then
					POS = POS + Vector(0, i, 0)
					found = true
					break
				end
			end
		end
		if not found then
			for i = 40, 300, 15 do
				if IsEmpty(POS + Vector(0, -i, 0)) then
					POS = POS + Vector(0, -i, 0)
					found = true
					break
				end
			end
		end
		if not found then
			for i = 40, 300, 15 do
				if IsEmpty(POS + Vector(-i, 0, 0)) then
					POS = POS + Vector(-i, 0, 0)
					--Yeah I found a nice position to put the player in!
					found = true
					break
				end
			end
		end
		-- If you STILL can't find it, you'll just put him on top of the other player lol
		if not found then
			POS = POS + Vector(0,0,70)
		end
	end
	return self.BaseClass:PlayerSelectSpawn(ply), POS
end

function GM:PlayerSpawn(ply)
	ply:CrosshairEnable()
	ply:SetHealth(tonumber(CfgVars["startinghealth"]) or 100)

	if CfgVars["crosshair"] == 0 then
		ply:CrosshairDisable()
	end
	
	--Kill any colormod
	local RP = RecipientFilter()
	RP:RemoveAllPlayers()
	RP:AddPlayer(ply)
	umsg.Start("DarkRPEffects", RP)
		umsg.String("colormod")
		umsg.String("0")
	umsg.End()
	RP:AddAllPlayers()
	
	-- If the player for some magical reason managed to respawn while jailed then re-jail the bastard.
	if RPArrestedPlayers[ply:SteamID()] and ply:GetTable().DeathPos then
		-- Not getting away that easily, Sonny Jim. -- Who the hell is Sonny Jim?
		if DB.RetrieveJailPos() then
			ply:Arrest()
		else
			Notify(ply, 1, 4, string.format(LANGUAGE.unable, "arrest"))
		end
	end	
	

	if CfgVars["babygod"] == 1 and not ply.IsSleeping then
		ply.Babygod = true
		ply:GodEnable()
		local r,g,b,a = ply:GetColor()
		ply:SetColor(r, g, b, 100)
		ply:SetCollisionGroup(  COLLISION_GROUP_WORLD )
		timer.Simple(CfgVars["babygodtime"] or 5, function()
			if not ValidEntity(ply) then return end
			ply.Babygod = false
			ply:SetColor(r, g, b, a)
			ply:GodDisable()
			ply:SetCollisionGroup( COLLISION_GROUP_PLAYER )
		end)
	end
	ply.IsSleeping = false
	
	GAMEMODE:SetPlayerSpeed(ply, CfgVars["wspd"], CfgVars["rspd"] )
	if ply:Team() == TEAM_CHIEF or ply:Team() == TEAM_POLICE then
		GAMEMODE:SetPlayerSpeed(ply, CfgVars["wspd"], CfgVars["rspd"] + 10 )
	end

	ply:Extinguish()
	if ply:GetActiveWeapon() and ValidEntity(ply:GetActiveWeapon()) then
		ply:GetActiveWeapon():Extinguish()
	end

	if ply.demotedWhileDead then
		ply.demotedWhileDead = nil
		ply:ChangeTeam(TEAM_CITIZEN)
	end
	
	ply:GetTable().StartHealth = ply:Health()
	gamemode.Call("PlayerSetModel", ply)
	gamemode.Call("PlayerLoadout", ply)
	DB.Log(ply:SteamName().." ("..ply:SteamID()..") spawned")
	
	local _, pos = self:PlayerSelectSpawn(ply)
	ply:SetPos(pos)
end

function GM:PlayerLoadout(ply)
	if RPArrestedPlayers[ply:SteamID()] then return end

	
	ply:GetTable().RPLicenseSpawn = true
	timer.Simple(1, removelicense, ply)
	
	local Team = ply:Team()
	
	ply:Give("keys")
	ply:Give("backpack")
	ply:Give("weapon_physcannon")
	ply:Give("gmod_camera")
	ply:Give("gmod_tool")
	ply:Give("weapon_physgun")
	
	if ply:HasPriv(ADMIN) and CfgVars["AdminsSpawnWithCopWeapons"] == 1 then
		ply:Give("door_ram")
		ply:Give("arrest_stick")
		ply:Give("unarrest_stick")
		ply:Give("stunstick")
		ply:Give("weaponchecker")
		if ply:IsSuperAdmin() then
			ply:Give("master_keypad")
			ply:Give("master_lockpick")
		end
	end

	for k,v in pairs(RPExtraTeams[Team].Weapons) do
		ply:Give(v)
	end
	
	-- Switch to prefered weapon if they have it
	local cl_defaultweapon = ply:GetInfo( "cl_defaultweapon" )
	
	if ply:HasWeapon( cl_defaultweapon ) then
		ply:SelectWeapon( cl_defaultweapon )
	end

	ply:SetHealth( 100 + (tonumber(GetLevel( ply )) * 10)  )
end

function GM:PlayerDisconnected(ply)
	self.BaseClass:PlayerDisconnected(ply)
	timer.Destroy(ply:SteamID() .. "jobtimer")
	timer.Destroy(ply:SteamID() .. "propertytax")
	
	/*
	for k, v in pairs(ents.FindByModel("models/props_lab/partsbin01.mdl")) do
		if v.SID == ply.SID then v:KillNow() end
	end
	for k, v in pairs(ents.FindByModel("models/props_wasteland/laundry_washer003.mdl")) do
		if v.SID == ply.SID then v:KillNow() end
	end
	for k, v in pairs(ents.FindByModel("models/props_wasteland/laundry_dryer002.mdl")) do
		if v.SID == ply.SID then v:KillNow() end
	end
	for k, v in pairs(ents.FindByModel("models/props_c17/consolebox01a.mdl")) do
		if v.SID == ply.SID then v:KillNow() end
	end
	for k, v in pairs(ents.FindByModel("models/weapons/w_c4_planted.mdl")) do
		if v.SID == ply.SID then v:KillNow() end
	end
	*/

	for k, v in pairs(ents.FindByClass("microwave")) do
		if v.SID == ply.SID then v:Remove() end
	end
	for k, v in pairs(ents.FindByClass("gunlab")) do
		if v.SID == ply.SID then v:Remove() end
	end
	for k, v in pairs(ents.FindByClass("letter")) do
		if v.SID == ply.SID then v:Remove() end
	end
	for k, v in pairs(ents.FindByClass("drug_lab")) do
		if v.SID == ply.SID then v:Remove() end
	end
	for k, v in pairs(ents.FindByClass("drug")) do
		if v.SID == ply.SID then v:Remove() end
	end
	
	for k,v in pairs(ents.GetAll()) do 
		if v:IsVehicle() and v.SID == ply.SID then
			v:Remove()
		end
	end
	vote.DestroyVotesWithEnt(ply)
	-- If you're arrested when you disconnect, you will serve your time again when you reconnect!
	if RPArrestedPlayers and RPArrestedPlayers[ply:SteamID()]then
		DB.StoreJailStatus(ply, math.ceil(GetGlobalInt("jailtimer")))
	end
	ply:UnownAll()
	DB.Log(ply:SteamName().." ("..ply:SteamID()..") disconnected")
end

local next_update_time
function GM:Think()
	FlammablePropThink()
	if EarthQuakeTest then EarthQuakeTest() end
end

function GM:GetFallDamage( ply, flFallSpeed )
	if GetConVarNumber("mp_falldamage") == 1 then
		return flFallSpeed / 15
	end
	return 10
end

local otherhooks = {}
function GM:PlayerSay(ply, text)--We will make the old hooks run AFTER DarkRP's playersay has been run.
	local text2 = text
	local callback
	text2, callback = RP_PlayerChat(ply, text2)
	if tostring(text2) == " " then text2, callback = callback, text2 end
	for k,v in SortedPairs(otherhooks, false) do
		if type(v) == "function" then
			text2 = v(ply, text2) or text2
		end
	end
	if isDedicatedServer() then
		ServerLog("\""..ply:Nick().."<"..ply:UserID()..">" .."<"..ply:SteamID()..">".."<"..team.GetName( ply:Team() )..">\" say \""..text.. "\"\n")
	end
	text2 = RP_ActualDoSay(ply, text2, callback) 
	return ""
end

function GM:InitPostEntity() 
	DB.Init()
	if not hook.GetTable().PlayerSay then return end
	for k,v in pairs(hook.GetTable().PlayerSay) do -- Remove all PlayerSay hooks, they all interfere with DarkRP's PlayerSay
		otherhooks[k] = v
		hook.Remove("PlayerSay", k)
	end
	for a,b in pairs(otherhooks) do
		if type(b) ~= "function" then
			otherhooks[a] = nil
		end
	end
end