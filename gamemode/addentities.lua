AddCustomShipment("Glock", "models/weapons/w_pist_glock18.mdl", "weapon_glock2", 0, 10, true, 2500, true, {})
AddCustomShipment("Fiveseven", "models/weapons/w_pist_fiveseven.mdl", "weapon_fiveseven2", 0, 10, true, 3075, true, {})
AddCustomShipment("P228", "models/weapons/w_pist_p228.mdl", "weapon_p2282", 0, 10, true, 2775, true, {})
AddCustomShipment("Desert eagle", "models/weapons/w_pist_deagle.mdl", "weapon_deagle2", 215, 10, true, 3225, true, {})

AddCustomShipment("Lockpick", "models/weapons/w_crowbar.mdl", "lockpick", 3750, 10, true, 450, true, {}) 
AddCustomShipment("KeypadCracker", "models/weapons/w_c4_planted.mdl", "keypad_cracker", 3750, 10, true, 450, true, {})


AddCustomShipment("Tmp", "models/weapons/w_smg_tmp.mdl", "weapon_tmp", 5000, 10, true, 15000, true, {})
AddCustomShipment("Mac10", "models/weapons/w_smg_mac10.mdl", "weapon_mac102", 8000, 10, true, 25000, true, {})
AddCustomShipment("MP5", "models/weapons/w_smg_mp5.mdl", "weapon_mp52", 10000, 10, true, 30000, true, {})
AddCustomShipment("M4", "models/weapons/w_rif_m4a1.mdl", "weapon_m42", 12000, 10, true, 35000, true, {})
AddCustomShipment("AK47", "models/weapons/w_rif_ak47.mdl", "weapon_ak472", 14000, 10, true, 40000, true, {})
AddCustomShipment("Pumpshotgun", "models/weapons/w_shot_m3super90.mdl", "weapon_pumpshotgun2", 85000, 10, true, 45000, true, {})
AddCustomShipment("Para", "models/weapons/w_mach_m249para.mdl", "weapon_para", 20000, 10, true, 50000, true, {})

AddCustomShipment("Tmp", "models/weapons/w_smg_tmp.mdl", "weapon_tmp", 100000, 10, false, nil, false, {TEAM_GUN}) 
AddCustomShipment("Mac10", "models/weapons/w_smg_mac10.mdl", "weapon_mac102", 200000, 10, false, nil, false, {TEAM_GUN}) 
AddCustomShipment("MP5", "models/weapons/w_smg_mp5.mdl", "weapon_mp52", 250000, 10, false, nil, false, {TEAM_GUN}) 
AddCustomShipment("M4", "models/weapons/w_rif_m4a1.mdl", "weapon_m42", 300000, 10, false, nil, false, {TEAM_GUN})
AddCustomShipment("AK47", "models/weapons/w_rif_ak47.mdl", "weapon_ak472", 350000, 10, false, nil, false, {TEAM_GUN}) 
AddCustomShipment("Pumpshotgun", "models/weapons/w_shot_m3super90.mdl", "weapon_pumpshotgun2", 400000, 10, false, nil, false, {TEAM_GUN}) 
AddCustomShipment("Para", "models/weapons/w_mach_m249para.mdl", "weapon_para", 450000, 10, false, nil, false, {TEAM_GUN}) 

AddCustomShipment("PrinterCooler", "models/weapons/w_c4_planted.mdl", "printer_cooler", 9000, 10, false, nil, false, {TEAM_PRINTSCI}) 
AddCustomShipment("PrinterBooster", "models/weapons/w_c4_planted.mdl", "printer_booster", 10000, 10, false, nil, false, {TEAM_PRINTSCI})
AddCustomShipment("PrinterRateIncreaser", "models/weapons/w_c4_planted.mdl", "printer_rateinc", 10000, 10, false, nil, false, {TEAM_PRINTSCI})

AddEntity("PrinterCooler", "printer_cooler", "models/weapons/w_c4_planted.mdl", 1000, 10, "/buyprintercooler")
AddEntity("PrinterBooster", "printer_booster", "models/weapons/w_c4_planted.mdl", 1500, 10, "/buyprinterbooster")
AddEntity("PrinterRateIncreaser", "printer_rateinc", "models/weapons/w_c4_planted.mdl", 1500, 10, "/buyprinterrateinc")

AddEntity("Money printer", "money_printer", "models/props_c17/consolebox01a.mdl", 250, 4, "/buymoneyprinter")

/*
How to add custom vehicles:
FIRST
go ingame, type rp_getvehicles for available vehicles!
then:
AddCustomVehicle(<One of the vehicles from the rp_getvehicles list>, <Price of the vehicle>, <OPTIONAL jobs that can buy the vehicle>)
Examples:
AddCustomVehicle("Jeep", "models/buggy.mdl", 100 )
AddCustomVehicle("Airboat", "models/airboat.mdl", 600, {TEAM_GUN})
AddCustomVehicle("Airboat", "models/airboat.mdl", 600, {TEAM_GUN, TEAM_MEDIC})

Add those lines under your custom shipments. At the bottom of this file or in data/CustomShipments.txt

HOW TO ADD CUSTOM SHIPMENTS:
AddCustomShipment("<Name of the shipment(no spaces)>"," <the model that the shipment spawns(should be the world model...)>", "<the classname of the weapon>", <the price of one shipment>, <how many guns there are in one shipment>, <OPTIONAL: true/false sold seperately>, <OPTIONAL: price when sold seperately>, < true/false OPTIONAL: /buy only = true> , OPTIONAL which classes can buy the shipment, OPTIONAL: the model of the shipment)

Notes:
MODEL: you can go to Q and then props tab at the top left then search for w_ and you can find all world models of the weapons!
CLASSNAME OF THE WEAPON
there are half-life 2 weapons you can add:
weapon_pistol
weapon_smg1
weapon_ar2
weapon_rpg
weapon_crowbar
weapon_physgun
weapon_357
weapon_crossbow
weapon_slam
weapon_bugbait
weapon_frag
weapon_physcannon
weapon_shotgun
gmod_tool

But you can also add the classnames of Lua weapons by going into the weapons/ folder and look at the name of the folder of the weapon you want.
Like the player possessor swep in addons/Player Possessor/lua/weapons You see a folder called weapon_posessor 
This means the classname is weapon_posessor

YOU CAN ADD ITEMS/ENTITIES TOO! but to actually make the entity you have to press E on the thing that the shipment spawned, BUT THAT'S OK!
YOU CAN MAKE GUNDEALERS ABLE TO SELL MEDKITS!

true/false: Can the weapon be sold seperately?(with /buy name) if you want yes then say true else say no

the price of sold seperate is the price it is when you do /buy name. Of course you only have to fill this in when sold seperate is true.


EXAMPLES OF CUSTOM SHIPMENTS(remove the // to activate it): */

//AddCustomShipment("HL2pistol", "models/weapons/W_pistol.mdl", "weapon_pistol", 500, 10, false, 200, false, {TEAM_GUN, TEAM_MEDIC})

// models/props_wasteland/laundry_washer001a.mdl

--EXAMPLE OF AN ENTITY(in this case a medkit)
--AddCustomShipment("bball", "models/Combine_Helicopter/helicopter_bomb01.mdl", "sent_ball", 100, 10, false, 10, false, {TEAM_GUN}, "models/props_c17/oildrum001_explosive.mdl")
--EXAMPLE OF A BOUNCY BALL:   		NOTE THAT YOU HAVE TO PRESS E REALLY QUICKLY ON THE BOMB OR YOU'LL EAT THE BALL LOL
--AddCustomShipment("bball", "models/Combine_Helicopter/helicopter_bomb01.mdl", "sent_ball", 100, 10, true, 10, true)
-- ADD CUSTOM SHIPMENTS HERE(next line):
AddEntity("Silver money printer", "money_printer_silver", "models/props_lab/partsbin01.mdl", 8000, 4, "/buymoneyprintersilver")
AddEntity("Gold money printer", "money_printer_gold", "models/props_wasteland/laundry_washer003.mdl", 10000, 4, "/buymoneyprintergold")
AddEntity("Platinum money printer", "money_printer_platinum", "models/props_lab/servers.mdl", 20000, 4, "/buymoneyprinterplatinum")
AddEntity("Diamond money printer", "money_printer_diamond", "models/props_wasteland/laundry_dryer002.mdl", 100000, 4, "/buymoneyprinterdiamond")