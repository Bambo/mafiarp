BannedProps = { }
function AddBannedProp(mdl) table.insert(BannedProps, mdl) end

AllowedProps = { }
function AddAllowedProp(mdl) table.insert(AllowedProps, mdl) end

-- HOW TO ADD BANNED PROPS (toggled with rp_bannedprops)(
-- AddBannedProp("MODEL NAME OF PROP")

AddBannedProp("models/props_combine/CombineTrain02b.mdl")
AddBannedProp("models/props_combine/CombineTrain02a.mdl")
AddBannedProp("models/props_combine/CombineTrain01.mdl")
AddBannedProp("models/Cranes/crane_frame.mdl")
AddBannedProp("models/props_wasteland/cargo_container01.mdl")
AddBannedProp("models/props_junk/TrashDumpster02.mdl")
AddBannedProp("models/props_c17/oildrum001_explosive.mdl")
AddBannedProp("models/props_canal/canal_bridge02.mdl")
AddBannedProp("models/props_canal/canal_bridge01.mdl")
AddBannedProp("models/props_canal/canal_bridge03a.mdl")
AddBannedProp("models/props_canal/canal_bridge03b.mdl")
AddBannedProp("models/props_wasteland/cargo_container01.mdl")
AddBannedProp("models/props_wasteland/cargo_container01c.mdl")
AddBannedProp("models/props_wasteland/cargo_container01b.mdl")
AddBannedProp("models/props_combine/combine_mine01.mdl")
AddBannedProp("models/props_junk/glassjug01.mdl")
AddBannedProp("models/props_c17/paper01.mdl")
AddBannedProp("models/props_junk/garbage_takeoutcarton001a.mdl")
AddBannedProp("models/props_c17/TrapPropeller_Engine.mdl")
AddBannedProp("models/props/cs_office/microwave.mdl")
AddBannedProp("models/Items/item_item_crate.mdl")
AddBannedProp("models/props_junk/gascan001a.mdl")
AddBannedProp("models/props_c17/consolebox01a.mdl")
AddBannedProp("models/props_buildings/building_002a.mdl")
AddBannedProp("models/props_phx/mk-82.mdl")
AddBannedProp("models/props_phx/cannonball.mdl")
AddBannedProp("models/props_phx/ball.mdl")
AddBannedProp("models/props_phx/amraam.mdl")
AddBannedProp("models/props_phx/misc/flakshell_big.mdl")
AddBannedProp("models/props_phx/ww2bomb.mdl")
AddBannedProp("models/props_phx/torpedo.mdl")
AddBannedProp("models/props/de_train/biohazardtank.mdl")
AddBannedProp("models/props_buildings/project_building01.mdl")
AddBannedProp("models/props_combine/prison01c.mdl")
AddBannedProp("models/props/cs_militia/silo_01.mdl")
AddBannedProp("models/props_phx/huge/evildisc_corp.mdl")
AddBannedProp("models/props_phx/misc/potato_launcher_explosive.mdl")
AddBannedProp("models/props_phx/oildrum001_explosive.mdl")

-- HOW TO ADD ALLOWEDP PROPS (toggled with rp_allowedprops)
-- AddAllowedProp("MODEL NAME OF PROP")

AddAllowedProp("BLARGSTEIN.mdl")
