// Copyright 2012
-- Crafting Materials list
-- bronze_ingot - used for general low level craftin ie. family_speedbooster_low
-- wire - used it all electronical shit
-- motor - NAME MIGHT CHANGE - used to increase players speed :D

local schematics = {}
schematics["speedbooster"] = { materials = { bronze_ingot = 4, wire = 4, motor = 1 }, entName="family_speedboost_low" }

function Craft( ply, command, args )
	-- i'll re-write this, i was tierd T_T
	local name = args[1]

	local schematic = schematics[name]
	local materials = schematic.materials
	local holder = {}
	local total = 0

	for k,v in pairs( materials ) do
		holder[k] = false
		total = total + 1
	end

	for name,quant in pairs( materials ) do
		local num = invGetItem( ply, name )
		if num >= quant then	-- he actually has the correct amount of item in his backpack!
			holder[name] = true
		end
	end

	local current = 0
	for k,v in pairs( holder ) do
		if v then current = current + 1 end
	end

	if current >= total then
		Create( ply, name )

		for name,quant in pairs( materials ) do
			local name = "material_" .. name
			for i=1, quant do
				invRemove( ply, name )
			end
		end
	else
		Notify( ply, 2, 1, "Sorry you do not have the correct materials!")
	end
end
concommand.Add("craft", Craft)

function Create( ply, name )
	local entname = schematics[name].entName
	local ent = ents.Create( entname )

	local trace = {}
	trace.start = ply:EyePos()
	trace.endpos = trace.start + ply:GetAimVector() * 85
	trace.filter = ply

	local tr = util.TraceLine(trace)
	ent:SetPos(tr.HitPos)
	ent:Spawn()
	ent.Owner = ply
end