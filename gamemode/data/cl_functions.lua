// Copyright 2011 TheBamfords <- lawl
function GetData( data )
	exp = data:ReadLong();
	level = data:ReadLong();
end
usermessage.Hook("SendData", GetData)

function GetFamily( data )
	family = data:ReadString();
end
usermessage.Hook("SendFamily", GetFamily)

function GetMaxExp( data )
	max_exp = data:ReadLong();
end
usermessage.Hook("SendMaxExp", GetMaxExp)


level = 1
exp = 1
max_exp = 100
family = ""
owner = false
invites = {}
players_table = {}
familys = {}

function GetLevel()
	return tonumber(level)
end

function GetCreds( data )
	family = data:ReadString();
	owner = data:ReadBool();
end
usermessage.Hook("SendCreds", GetCreds)

function GetMaxFamilyExp( )
	if familys[family] ~= nil then
		local level = familys[family].level
		if level ~= nil then
			return tonumber((level+1000) * math.pow(2, 11/3))
		elseif level == nil then
			return 1
		end
	else
		return 1000
	end
end

function GetGlobalData( data )
	local level = data:ReadLong()
	local family = data:ReadString()
	local steamid = data:ReadString()
	players_table[steamid] = {family = family, level = level}
end
usermessage.Hook("SendGlobalData", GetGlobalData)

function GetGlobalDataFamily( data )
	local exp = data:ReadLong()
	local level = data:ReadLong()
	local name = data:ReadString()
	familys[name] = {exp = exp, level = level}
end
usermessage.Hook("SendGlobalDataFamily", GetGlobalDataFamily)

function GetInvite( data )
	local family = data:ReadString()
	if table.getn( invites ) < 5 then
		table.insert(invites, family)
		LocalPlayer():ChatPrint("Recived an invite to the family: " .. family)
	else
		LocalPlayer():ChatPrint("Recived an invite to the family: " .. family .. " But your invite box is full! please clear it from the F4 menu")
	end
end
usermessage.Hook("SendInvite", GetInvite)

include("cl_command_menu.lua")
include("cl_inventory.lua")