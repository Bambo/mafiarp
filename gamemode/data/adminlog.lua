// Copyright 2012

log_table = {}

function log( target, string )
	local len = #log_table + 1

	if len > 32 then
		for k,v in ipairs( log ) do
			log_table[k-1] = v
			log_table[k] = nil
		end
	else
		log_table[len] = { target:Nick(), string }
	end

	logSend()
end

function logSend()
	for _, v in pairs(player.GetAll()) do
		if v:IsAdmin() then
			umsg.Start( "AdminLog", v )
				umsg.String(glon.encode(log_table))
			umsg.End()
		end
	end
end