// Copyright 2012
include("adminlog.lua")

function admin( ply, command, args )
	-- admin    cmd      player     reason   time
	-- command, args[1], args[2], args[3], args[4]

	local command = args[1] -- kick

	if ply:IsAdmin() then

		-- admin kick name "reason"
		if command == 'kick' then
			local target = FindPlayer( args[2] )
			local reason = args[3]
			target:Kick( reason )
		end

		-- admin ban NAME "reason now" time
		if command == 'ban' then
			local target = FindPlayer( args[2] )
			local reason = args[3]
			local time = args[4]

			if time == nil then time = 0 end
			if reason == nil then reason = 'No Reason' end
			 
			target:Ban(args[4], args[3])
			target:Kick( args[3] )
		end

		-- admin kickall
		if command == 'kickall' then
			for _, v in pairs( player.GetAll() ) do
				v:Kick( 'server restart' )
			end
		end

		-- admin hide
		if command == 'hide' then
			ply:SetRenderMode( RENDERMODE_NONE )
			ply:SetColor( 255, 255, 255, 0 )
			ply:GodEnable()
		end

		-- admin unhide
		if command == 'unhide' then
			ply:SetRenderMode( RENDERMODE_NORMAL )
			ply:SetColor( 255, 255, 255, 255 )
			ply:GodDisable()
		end

		-- admin teleport NAME
		if command == 'tele' then
			local target = FindPlayer(args[2])
			Goto( ply, target )
		end

		-- admin explode bambo
		if command == 'explode' then
			local target = FindPlayer(args[2])
			local explosive = ents.Create("env_explosion")
			explosive:SetPos( target:GetPos() )
			explosive:SetOwner( target )
			explosive:Spawn()
			explosive:SetKeyValue( "iMagnitude", "1" )
			explosive:Fire( "Explode", 0, 0 )
			explosive:EmitSound( "ambient/explosions/explode_4.wav", 500, 500 )

			target:SetVelocity( Vector(0, 0, 400 ) )
			target:Kill()
		end

		if command == 'award' then
			local target = FindPlayer( args[2] )
			local nameofitem = args[3]
			local num = args[4]
			invAdd( target, nameofitem, false, num )
		end

		-- admin innode
		if command == 'innode' then
			SpawnEnt( ply, "event_node_in" )
		end

		if command == 'outnode' then
			SpawnEnt( ply, "event_node_out" )
		end

	end

	if ply:IsSuperAdmin() then
		if command == 'givelevel' then
			if args[2] == nil then 
				AddExp( 100000000, ply )
			else
				local target = FindPlayer( args[2] )
				AddExp( 100000000, target )
			end
		end
	end
end
concommand.Add( "admin", admin )

function FindPosition( ply )
	local size = Vector( 32, 32, 72 )
	local StartPos = ply:GetPos() + Vector(0,0,size.z/2)

	for _,v in pairs(positions) do
		local Pos = StartPos + v * size * 1.5

		local tr = {}
		tr.start = Pos
		tr.endpos = Pos
		tr.mins = size / 2 * -1
		tr.maxs = size / 2
		local trace = util.TraceHull( tr )

		if(!trace.Hit) then
			return Pos - Vector(0, 0, size.z/ 2)
		end
	end
end

function Goto( ply, target )
	if( ply:InVehicle() ) then ply:ExitVehicle() end
	if( ply:GetMoveType()  == MOVETYPE_NOCLIP ) then
		ply:SetPos( target:GetPos() + target:GetForward() * 45 )
	else
		local Pos = FindPosition( target )
		if (Pos) then
			ply:SetPos( Pos )
		else
			ply:SetPos( target:GetPos() + Vector( 0, 0, 72 ) )
		end
	end
end

function SpawnEnt( ply, entname )
	if entname == "event_node_out" then
		for k,v in pairs( ents.FindByClass("event_node_out") ) do
			v:Remove()
		end
	elseif entname == "event_node_in" then
		for k,v in pairs( ents.FindByClass("event_node_in") ) do
			v:Remove()
		end
	end
	
	local tr = ply:GetEyeTrace() -- tr now contains a trace object
	local ent = ents.Create(entname)
	ent:SetPos(tr.HitPos)
	ent:Spawn()
end

function report( ply, command, args )
	-- report NAME REASON
	local string = args[2]
	local localply = FindPlayer( args[1] )
	local len = string.len( string )
	local award = args[3]

	if localply ~= nil then
		if len < 60 then
			log( localply, localply:Nick() .. " - " .. string )
			if award then
				Notify( ply, 2, 3, "Reported!" )
			else
				Notify( ply, 2, 3, "Awarded!" )
			end
		else
			Notify( ply, 2, 3, "Sorry too long, only 60 characters allowed")
		end
	else
		Notify( ply, 2, 3, "That player doesn't exist!" )
	end
end
concommand.Add( "report", report )