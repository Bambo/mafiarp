// Copyright 2012
local zombies = {}

function CreateInvasionTimer()
	timer.Create( "InvasionTimer", 3600, 0, StartInvasion )
end
hook.Add("Initialize", "InvasionTimer", CreateInvasionTimer )

function StartInvasion()
	zombies[1] = ents.Create("npc_zombie")
	zombies[1]:SetPos( Vector( 2642, -2246, -140 ) )
	zombies[1]:Spawn()

	zombies[2] = ents.Create("npc_zombie")
	zombies[2]:SetPos( Vector( 2720, -2117, -140 ) )
	zombies[2]:Spawn()

	zombies[3] = ents.Create("npc_zombie")
	zombies[3]:SetPos( Vector( 2575, -2057, -140 ) )
	zombies[3]:Spawn()

	zombies[4] = ents.Create("npc_zombie")
	zombies[4]:SetPos( Vector( 2641, -1820, -140 ) )
	zombies[4]:Spawn()

	zombies[5] = ents.Create("npc_zombie")
	zombies[5]:SetPos( Vector( 2503, -2239, -140 ) )
	zombies[5]:Spawn()

	timer.Create( "ClearTimer", 1800, 1, ClearInvasion )
end

function ClearInvasion()
	for k,v in pairs( zombies ) do
		if v:IsValid() then
			v:Remove()
		end
	end

	zombies = {} -- reset the table
end

function GM:EntityTakeDamage( ent, inflicator, attacker, amount, dmginfo )
	-- 10 is the default damage inflicted by the npc_zombie
	-- lets fuck them up and make it so that it takes 2 hits to kill them >:)
	if dmginfo:IsDamageType( DMG_SLASH ) then
		if ent:IsPlayer() and dmginfo:GetInflictor():GetClass() == "npc_zombie" then
			local plydmg = ent:GetMaxHealth() / 2
			dmginfo:SetDamage( plydmg )
		end
	end
end