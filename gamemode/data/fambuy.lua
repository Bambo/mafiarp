// Copyright 2011 TheBamfords <- lawl
local family_shop = {}
family_shop["/buyhpboostlow"] = { level = 1, cost = 100000, ent = "family_hpboost_low" }
family_shop["/buyhpboostmed"] = { level = 4, cost = 500000, ent = "family_hpboost_med" }
family_shop["/buyhpboosthigh"] = { level = 10, cost = 1000000, ent = "family_hpboost_high" }

family_shop["/buybigdaddy"] = { level = 10, cost = 30000000, ent = "big_daddy" }

family_shop["/buyspeedboostlow"] = { level = 2, cost = 100000, ent = "family_speedboost_low" }
family_shop["/buyspeedboostmed"] = { level = 6, cost = 500000, ent = "family_speedboost_low" }
family_shop["/buyspeedboosthigh"] = { level = 10, cost = 1000000, ent = "family_speedboost_high" }

function familyBuy( ply, command, args )
	if tonumber(GetFamilyLevel( ply )) >= family_shop[args[1]].level then	// family level is AOK
		if ply:CanAfford( family_shop[args[1]].cost ) then

			ply:AddMoney( -family_shop[args[1]].cost )

			local ent = ents.Create(family_shop[args[1]].ent)

			local trace = {}
			trace.start = ply:EyePos()
			trace.endpos = trace.start + ply:GetAimVector() * 85
			trace.filter = ply

			local tr = util.TraceLine(trace)
			ent:SetPos(tr.HitPos)
			ent:Spawn()
			ent.Owner = ply
		else
			ply:ChatPrint("Cannot afford")
		end
	else
		ply:ChatPrint("family level is too low")
	end
end
concommand.Add( "fambuy", familyBuy )


function spawnEntityPlz( ply, command, args )
	if ply:IsSuperAdmin() then
		local tr = ply:GetEyeTrace() -- tr now contains a trace object
		local ent = ents.Create(args[1])
		ent:SetPos(tr.HitPos)
		ent:Spawn()
	end
end
concommand.Add( "SpawnEntity", spawnEntityPlz )