-- Player
function send_data( ply )
	umsg.Start( "SendData", ply );
		umsg.Long( players_table[ply:SteamID()].exp );
		umsg.Long( players_table[ply:SteamID()].level );
	umsg.End();
	SendCreds( ply )
end

function send_global_data()
	for k, v in pairs(players_table) do
		umsg.Start( "SendGlobalData" )
			umsg.Long(v.level)			-- level
			umsg.String(v.family)		-- family
			umsg.String(k)			-- steamid
		umsg.End()
	end
	
	for k, v in pairs(familys) do
		umsg.Start( "SendGlobalDataFamily" )
			umsg.Long(v.exp)			-- exp
			umsg.Long(v.level)		-- level
			umsg.String(k)			-- name
		umsg.End()
	end
end

function send_max_exp ( ply )
	umsg.Start( "SendMaxExp", ply );
		umsg.Long( GetMaxExp( ply ) );
	umsg.End();
end


function GetMaxExp( ply )
	local level = tonumber(players_table[ply:SteamID()].level)
	if level ~= nil then
		return tonumber((level+300) * math.pow(2, level/3))
	end
end

function load_info( ply )
	players_table[ply:SteamID()].exp = sql.QueryValue("SELECT exp FROM player_info WHERE unique_id = '" .. ply:SteamID() .. "'")
	players_table[ply:SteamID()].level = sql.QueryValue("SELECT level FROM player_info WHERE unique_id = '" .. ply:SteamID() .. "'")
	players_table[ply:SteamID()].family = sql.QueryValue("SELECT family FROM player_info WHERE unique_id = '" .. ply:SteamID() .. "'")
	if players_table[ply:SteamID()].family ~= "" then
		local owner = sql.QueryValue("SELECT owner FROM familys WHERE name = '" .. sql.SQLStr(players_table[ply:SteamID()].family) .. "'")
		if owner == ply:SteamID() then
			players_table[ply:SteamID()].owner = true
		end
	end
	send_data( ply )
	send_max_exp( ply )
	SendCreds( ply )
end

function saveStat( ply )
	if not IsValid(ply) then return end
	local steamID = ply:SteamID()
	local exp = players_table[steamID].exp
	local level = players_table[steamID].level
	local family = players_table[steamID].family
	sql.Query("UPDATE player_info SET exp = " .. exp .. ", level = " .. level .. ", family = '" .. family .. "' WHERE unique_id = '" .. steamID .. "'")
end

function GlobalSave()
	for k,v in pairs(familys) do
		sql.Query("UPDATE familys SET exp = " .. v.exp .. ", level = " .. v.level .. ", members = " .. v.members .. " WHERE name = '" .. sql.SQLStr(k) .. "'")
	end
end

-- Server
function tables_exist()
	if sql.TableExists("player_info") and sql.TableExists("familys") then
		print("tables already exist!")
	else
		if (!sql.TableExists("player_info")) then
			local query = "CREATE TABLE player_info ( unique_id varchar(255), exp int, level int, family varchar(255) )"
			local result = sql.Query(query)
			if (sql.TableExists("player_info")) then
				print("Success! table created \n")
			else
				print("Somthing went wrong with the player_info query! \n")
				print( sql.LastError( result ) .. "\n" )
			end	
		end
		
		if (!sql.TableExists("familys")) then
			local query = "CREATE TABLE familys ( name varchar(255), owner varchar(255), exp int, level int, members int )"
			local result = sql.Query(query)
			if (sql.TableExists("familys")) then
				print("Success! table created \n")
			else
				print("Somthing went wrong with the player_info query! \n")
				print( sql.LastError( result ) .. "\n" )
			end	
		end
	end
 
end

function new_player( ply )
	sql.Query( "INSERT INTO player_info (`unique_id`, `exp`, `level`, `family`) VALUES ('" .. ply:SteamID() .. "', '1', '1', '')" )
	send_data( ply )
	send_max_exp( ply )
	-- FirstJoinMySQL( ply )
end
 
function player_exists( ply )
	local steamID = ply:SteamID()
	local result = sql.Query("SELECT level FROM player_info WHERE unique_id = '" .. steamID .. "'")
	if result ~= nil then
		print("Loading player")
		load_info(ply) -- We will call this to retrieve the stats
		send_max_exp( ply )
		if familys[players_table[ply:SteamID()].family] == nil and players_table[ply:SteamID()].family ~= "" then
			LoadFamily( players_table[ply:SteamID()].family )
		end
	else
		print("creating new player")
		new_player( ply ) -- Create a new player :D
	end
end

function loadPermaEnts()
	-- YOU HAVE TO SUBTRACT 80 from the Y to make it not in the floor :S
	local pole = ents.Create("capture_pole")
	pole:SetPos(Vector(2631.950195, -1543.604370,-210))
	pole:Spawn()

	local pole = ents.Create("capture_pole")
	pole:SetPos(Vector(2392.300537, 855.233215, -210))
	pole:Spawn()

	local pole = ents.Create("capture_pole")
	pole:SetPos(Vector(4142.971191, 3740.215088, -210))
	pole:Spawn()

	local pole = ents.Create("capture_pole")
	pole:SetPos(Vector(258.670319, 679.947266, -210))
	pole:Spawn()

	local ent = ents.Create("schematic_helper")
	ent:SetPos(Vector(-1370.801758, -131.968750, -152))
	-- 5.609976
	ent:SetAngles(Angle(0, -89.519806, 0))
	ent:Spawn()

	local ent = ents.Create("schematic_helper")
	ent:SetPos(Vector( 2110.001221, 3764.724365, -152))
	--10.889986
	ent:SetAngles(Angle(0, -1.159908, 0))
	ent:Spawn()

	local ent = ents.Create("schematic_helper")
	ent:SetPos(Vector(8.031251, 4176.311523, -152))
	-- 4.729983
	ent:SetAngles(Angle(0, -1.299872, 0))
	ent:Spawn()
end

function Initialize()
	tables_exist()
	invites = {}
	familys = {}
	donators = {}

	loadDonators()
	loadPermaEnts()

	players_table = {}

	math.randomseed( os.time() )

	timer.Create("invitesreset", 360, 0, function()
		invites = {}
	end)

	timer.Create("GlobalSave", 60, 0, function()
		GlobalSave()
	end)

	positions = {}
	for i=0,360,45 do table.insert( positions, Vector(math.cos(i),math.sin(i),0) ) end -- Around
	table.insert( positions, Vector(0,0,1) ) -- Above
end

function GiveWeps( ply )
	ply:Give("weapon_physgun") 
	ply:Give("weapon_physcannon")
	ply:Give("gmod_tool")
	ply:Give("gmod_camera")
	ply:Give("keys")
	ply:Give("backpack")
end

function PlayerInitialSpawn( ply )
	timer.Create("SteamIDDelay", 1, 1, function()
		players_table[ply:SteamID()] = {exp = 1, level = 1, family = "", donator = false, owner = false }
		
		ply.maxbonds = 6
		
		player_exists( ply )
		send_global_data()
		GiveWeps( ply )		-- for some reason if you take this out, no weapons are given to players at loadout?

		timer.Create("MoreDelay", 2, 1, function() 
			hook.Add( "PlayerSpawn", "playerspawny", Playerspawn )
		end)

		
		timer.Create("SaveStat-" .. ply:SteamID(), 15, 0, function() 
			saveStat( ply )
			send_global_data()
		end)
		timer.Create("checkLevel-" .. ply:SteamID(), 60, 0, function() checkLevel( ply ) end)
	end)
end

function fPlayerDisconnect( ply )
	ply:UnownAll()

	local steamID = ply:SteamID()
	local exp = players_table[ply:SteamID()].exp
	local level = players_table[ply:SteamID()].level
	local family = players_table[ply:SteamID()].family
	
	sql.Query("UPDATE player_info SET exp = " .. exp .. ", level = " .. level .. ", family = '" .. family .. "' WHERE unique_id = '".. steamID .."'")
	
	players_table[ply:SteamID()] = nil

	
	timer.Destroy("UpdateMYSQL-" .. ply:SteamID())
	timer.Destroy("checkLevel-" .. ply:SteamID())
	timer.Destroy("SaveStat-" .. ply:SteamID())
end

function Playerspawn( ply )
	send_max_exp( ply )
end

hook.Add( "PlayerInitialSpawn", "PlayerInitialSpawn", PlayerInitialSpawn )
hook.Add( "Initialize", "Initialize", Initialize )
hook.Add( "PlayerDisconnected", "playerdisconnected", fPlayerDisconnect ) 

/*
SERVER START -> load all family which have been saved to SQLLite
SERVER TICK (5mins) -> save all the gangs info into SQL

CLIENT JOIN -> Load their family
CLIENT CREATE FAMILY -> create new gang with specifications into the temporary table
CLIENT DELETE FAMILY -> remove family with index of ID
*/
