inventory = {}

function recieveInv( data )
	inventory = glon.decode( data:ReadString() )
end
usermessage.Hook("SendInventory", recieveInv)

function inventory()
	local DFrame3
	local commandList

	DFrame3 = vgui.Create('DFrame')
	DFrame3:SetSize( 640, 180 )
	DFrame3:SetPos( 110, 50 )
	DFrame3:SetTitle( 'Backpack' )
	DFrame3:SetSizable( false )
	DFrame3:MakePopup()

	commandList = vgui.Create( "DPanelList" )
	commandList:SetParent( DFrame3 )
	commandList:SetSpacing( 5 )
	commandList:SetPos( 20, 40 )
	commandList:SetWidth( 600 )
	commandList:SetHeight( 120 )
	commandList:EnableHorizontal( true )
	commandList:EnableVerticalScrollbar( true )

	for k,v in pairs( inventory ) do
		for i=1, v[1] do
			local spawnIcon = vgui.Create( "SpawnIcon" )
			spawnIcon:SetModel( v[2] )
			spawnIcon.DoClick = function()
				RunConsoleCommand( "InvSpawn", k )
				DFrame3:Remove()
			end
			commandList:AddItem( spawnIcon )
		end
	end
end
usermessage.Hook("OpenInventory", inventory)

local on = false
function OpenHelpSch()
	if not on then
		local DFrameHelper = vgui.Create("DFrame")
		DFrameHelper:SetPos( 50, 50 )
		DFrameHelper:SetSize( ScrW() - 100, ScrH() - 100 )
		DFrameHelper:SetTitle( "Schematic Library - Press F3 to enable cursor" )
		DFrameHelper:SetSizable( false )

		local HelperHTML = vgui.Create("HTML")
		HelperHTML:SetParent( DFrameHelper )
		HelperHTML:SetPos( 20, 30 )
		HelperHTML:SetSize( ScrW() - 140, ScrH() - 140 )
		HelperHTML:OpenURL("http://www.bambofy.co.uk/mafiarp/schematichelp.html")
		on = true
		timer.Simple( 2, unlock )
	end
end

function unlock()
	on = false
end
usermessage.Hook("OpenSchematicHelper", OpenHelpSch)