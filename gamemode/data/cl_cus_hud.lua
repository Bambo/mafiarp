// Copyright 2011 TheBamfords <- lawl

function hud()
local max_exp = (level+300) * math.pow(2, level/3)

local percent = exp/max_exp
local total = (ScrW() - 400) * percent
surface.SetDrawColor(Color( 255, 255, 255, 255 ));
surface.DrawOutlinedRect( 229, ScrH()-16, ScrW() - 400, 12 );  --Draw box

draw.RoundedBox(4, 230, ScrH()-15, total, 10, Color(125, 125, 255, 200))  --Draw fill

draw.RoundedBox( 4, 225, ScrH()-40, 85, 20, Color( 100, 100, 100, 200 )  );  -- level background
draw.SimpleText("Level:", "TargetID", 229, ScrH() - 42, Color(255,255,255,255))  --level
draw.SimpleText(level, "TargetID", 279, ScrH() - 42, Color(255,255,255,255))

surface.SetDrawColor(255, 255, 255,255);    -- Draws lines on exp bar

end 
hook.Add("HUDPaint", "MyHudName", hud)
