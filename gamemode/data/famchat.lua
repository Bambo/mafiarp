// Copyright 2011 TheBamfords <- lawl
function familyChat( ply, args )

	if args == "" then return "" end
	local col = Color( 255, 255, 0 )
	local col2 = Color(255,255,255,255)
	if not ply:Alive() then
		col2 = Color(255,200,200,255)
	end

	for k,v in pairs(player.GetAll()) do
		if players_table[v:SteamID()].family == players_table[ply:SteamID()].family then
			TalkToPerson(v, col, "[" .. players_table[v:SteamID()].family .. "] "..ply:Name(), col2, args, ply)
		end
	end
	return ""

end
AddChatCommand("/family", familyChat, true)
AddChatCommand("/fam", familyChat, true)