-- this is where we store all the peoples inventorys while they are in game...
inventorys = {}

function invAdd( ply, name, model, num )
	if num == nil then num = 1 end
	if not model then model = "models/noesis/donut.mdl" end
	local size = invGetSize( ply ) + num

	if size <= 20 then
		local inventory = inventorys[ply:SteamID()]
		for i=1, num do
			local content = inventory[name]
			if content == nil then -- they don't have the item
				inventory[name] = { 1, model } 
			else -- they have one
				inventory[name][1] = inventory[name][1] + 1
			end
		end
		invSendClient( ply )
	end
end

function invRemove( ply, name )
	local inventory = inventorys[ply:SteamID()]
	local content = inventory[name]
	if content ~= nil then -- content doesn't exist
		if content == 1 then -- make it nil
			inventory[name] = nil
		else
			inventory[name][1] = inventory[name][1] - 1
		end
	end
	invSendClient( ply )
end

function invSpawn( ply, command, args )
	-- InvSpawn clasname

	local name = args[1]
	local inventory = inventorys[ply:SteamID()]
	if inventory[name] ~= nil then
		if inventory[name][1] >= 1 then
			local ent = ents.Create( name )
			local trace = {}
			trace.start = ply:EyePos()
			trace.endpos = trace.start + ply:GetAimVector() * 85
			trace.filter = ply

			local tr = util.TraceLine(trace)
			ent:SetPos(tr.HitPos)
			ent:Spawn()
			ent.Owner = ply

			invRemove( ply, name )

			Notify( ply, 2, 3, "You have just taken an item!")
		end
	end
end
concommand.Add("InvSpawn", invSpawn)

function invCreate( ply )
	inventorys[ply:SteamID()] = {}	-- not sure why but i think it wont save unless the table has contents?
	file.Write( "mafiarp/inventorys/" .. ply:SteamID() .. ".txt", glon.encode(  inventorys[ply:SteamID()] ) )	-- save eet
	invSendClient( ply )
end

function invDelete( ply )	-- called on disconnect
	inventorys[ply:SteamID()] = nil
	invSave( ply )
end
hook.Add( "PlayerDisconnected", "playerdisconnectedinventory", invDelete )

function invLoad( ply )
	if file.Exists( "mafiarp/inventorys/" .. ply:SteamID() .. ".txt") then -- already created
		invLoadFromFile( ply )
	else
		invCreate( ply )
	end
end
hook.Add( "PlayerInitialSpawn", "PlayerInitialSpawnInventory", invLoad )

function invLoadFromFile( ply )
	local contents = file.Read("mafiarp/inventorys/" .. ply:SteamID() .. ".txt" )
	inventorys[ply:SteamID()] = glon.decode( contents )
	invSendClient( ply )
end

function invSave( ply )
	file.Write( "mafiarp/inventorys/" .. ply:SteamID() .. ".txt", glon.encode(  inventorys[ply:SteamID()] ) )
end

function invSendClient( ply )
	umsg.Start( "SendInventory", ply )
		umsg.String( glon.encode( inventorys[ply:SteamID()] ) )
	umsg.End()
end

function invGetSize( ply )
	local size = 0
	for k,v in ipairs( inventorys[ply:SteamID()] ) do
		size = size + v[1]
	end
	return size
end

function invGet( ply )
	return inventorys[ply:SteamID()]
end

function invGetItem( ply, name )
	local num = 0
	local name = "material_" .. name	-- i want to keep the entity folder cleanr
	for k,v in pairs( invGet(ply) ) do
		if k == name then
			num = v[1]
		end
	end
	return num
end

function OpenInv( ply )
	umsg.Start( "OpenInventory", ply )
	umsg.End()
end
concommand.Add("OpenInv", OpenInv)