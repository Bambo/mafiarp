// Copyright 2012
-- I see you trying to copy my code, we don't mind. Infact we find it quite flattering that you would try to copy our gamemode :)

log = {}

function PlayerList()
	local DermaPanel = vgui.Create( "DFrame" )
	DermaPanel:SetPos( 50,50 )
	DermaPanel:SetSize( 350,650 )
	DermaPanel:SetTitle( "SAdmin Menu" )
	DermaPanel:SetVisible( true )
	DermaPanel:SetDraggable( true )
	DermaPanel:ShowCloseButton( true )
	DermaPanel:MakePopup()

	local commandList = vgui.Create( "DPanelList" )
	commandList:SetParent( DermaPanel )
	commandList:SetSpacing( 5 )
	commandList:SetPos( 50, 50 )
	commandList:SetWidth( 250 )
	commandList:SetHeight( 550 )
	commandList:EnableHorizontal( false )
	commandList:EnableVerticalScrollbar( true )

	if LocalPlayer():IsAdmin() then

		local hide = vgui.Create( "DButton" )
		hide:SetText("Hide")
		hide.DoClick = function() RunConsoleCommand( "admin", "hide" ) end
		commandList:AddItem( hide )

		local unhide = vgui.Create( "DButton" )
		unhide:SetText("UnHide")
		unhide.DoClick = function() RunConsoleCommand( "admin", "unhide" ) end
		commandList:AddItem( unhide )

		local checkhide = vgui.Create( "DButton" )
		checkhide:SetText("Check Hide")
		checkhide.DoClick = function() 
			local r,g,b,a = LocalPlayer():GetColor()
			if a == 0 then
				LocalPlayer():ChatPrint("You are hidden")
			else
				LocalPlayer():ChatPrint("You are unhidden")
			end
		end
		commandList:AddItem( checkhide )

		local log = vgui.Create( "DButton" )
		log:SetText("Log")
		log.DoClick = function() AdminLog() end
		commandList:AddItem( log )

		local eventcmd = vgui.Create( "DButton" )
		eventcmd:SetText("Event Commands")
		eventcmd.DoClick = function() EventCommands() end
		commandList:AddItem( eventcmd )
	end
		 
	local DermaListView = vgui.Create("DListView")
	DermaListView:SetMultiSelect(false)
	DermaListView:SetHeight( 600 )
	DermaListView:AddColumn("Name") -- Add column
	DermaListView:AddColumn("SteamID") -- another column
	for k,v in pairs(player.GetAll()) do
	    DermaListView:AddLine(v:Nick(), v:SteamID()) -- Add lines
	end
	DermaListView.OnClickLine = function(parent, line, isselected)
		Popup( line:GetValue(1), line:GetValue(2) )
	end
	commandList:AddItem(DermaListView)
end
usermessage.Hook( "OpenAdminMenu", PlayerList )

function Popup( nick, steamid )
	local DermaPanel = vgui.Create( "DFrame" )
	DermaPanel:SetPos( 300,300 )
	DermaPanel:SetSize( 350, 350 )
	DermaPanel:SetTitle( "SAdmin Command Centre" )
	DermaPanel:SetVisible( true )
	DermaPanel:SetDraggable( true )
	DermaPanel:ShowCloseButton( true )
	DermaPanel:MakePopup()
	
	local commandList = vgui.Create( "DPanelList" )
	commandList:SetParent( DermaPanel )
	commandList:SetSpacing( 5 )
	commandList:SetPos( 50, 50 )
	commandList:SetWidth( 250 )
	commandList:SetHeight( 350 )
	commandList:EnableHorizontal( false )
	commandList:EnableVerticalScrollbar( true )


	if LocalPlayer():IsAdmin() then
		local reason = vgui.Create( "DTextEntry" )
		reason:SetEnterAllowed( true )
		reason:SetText("Variable 1 ( Reason/Item Name/Check the forums... )")
		commandList:AddItem( reason )

		local time = vgui.Create( "DTextEntry" )
		time:SetEnterAllowed( true )
		time:SetText("Variable 2 ( Time/Item Amount/Check the forums! )")
		commandList:AddItem( time )

		local kick = vgui.Create( "DButton" )
		kick:SetText("Kick")
		kick.DoClick = function() RunConsoleCommand( "admin", "kick", nick, reason:GetValue() .. " - " .. LocalPlayer():Nick() ) end
		commandList:AddItem( kick )

		local explode = vgui.Create( "DButton" )
		explode:SetText("Explode")
		explode.DoClick = function() RunConsoleCommand( "admin", "explode", nick ) end
		commandList:AddItem( explode )

		local ban = vgui.Create( "DButton" )
		ban:SetText("Ban")
		ban.DoClick = function() RunConsoleCommand( "admin", "ban", nick, reason:GetValue() .. " - " .. LocalPlayer():Nick(), time:GetValue() ) end
		commandList:AddItem( ban )

		local tele = vgui.Create( "DButton" )
		tele:SetText("Tele")
		tele.DoClick = function() RunConsoleCommand( "admin", "tele", nick ) end
		commandList:AddItem( tele )
	else
		local report = vgui.Create( "DTextEntry" )
		report:SetEnterAllowed( true )
		report:SetText( "Report reason..." )
		commandList:AddItem( report )

		local gobut = vgui.Create( "DButton" )
		gobut:SetText("Report")
		gobut.DoClick = function() RunConsoleCommand("report", nick, report:GetValue() ) end
		commandList:AddItem( gobut )
	end
end

function AdminLog()
	if LocalPlayer():IsAdmin() then
		local DermaPanel = vgui.Create( "DFrame" )
		DermaPanel:SetPos( 300, 300 )
		DermaPanel:SetSize( 600, 250 )
		DermaPanel:SetTitle( "SAdmin Log" )
		DermaPanel:SetVisible( true )
		DermaPanel:SetDraggable( true )
		DermaPanel:ShowCloseButton( true )
		DermaPanel:MakePopup()
		

		local DermaListView = vgui.Create("DListView")
		DermaListView:SetMultiSelect(false)
		DermaListView:SetPos( 20, 20 )
		DermaListView:SetSize( 560, 210 )
		DermaListView:SetParent( DermaPanel )
		DermaListView:AddColumn("Name") -- Add column
		DermaListView:AddColumn("Log") -- another column
		for k,v in pairs(log) do
		    DermaListView:AddLine(v[1], v[2]) -- Add lines
		end
		DermaListView.OnClickLine = function(parent, line, isselected)
			Popup( line:GetValue(1), line:GetValue(2) )
		end
	end
end

function AdminLogLoad( data )
	log = glon.decode( data:ReadString() )
end
usermessage.Hook("AdminLog", AdminLogLoad)

function EventCommands()
	local DermaPanel = vgui.Create( "DFrame" )
	DermaPanel:SetPos( 300,300 )
	DermaPanel:SetSize( 350, 350 )
	DermaPanel:SetTitle( "SAdmin Event Commands" )
	DermaPanel:SetVisible( true )
	DermaPanel:SetDraggable( true )
	DermaPanel:ShowCloseButton( true )
	DermaPanel:MakePopup()
	
	local commandList = vgui.Create( "DPanelList" )
	commandList:SetParent( DermaPanel )
	commandList:SetSpacing( 5 )
	commandList:SetPos( 50, 50 )
	commandList:SetWidth( 250 )
	commandList:SetHeight( 350 )
	commandList:EnableHorizontal( false )
	commandList:EnableVerticalScrollbar( true )


	if LocalPlayer():IsAdmin() then
		local award = vgui.Create("DButton")
		award:SetText("Award")
		award.DoClick = function()
			RunConsoleCommand( "admin", "award", nick, reason:GetValue(), time:GetValue() )
			RunConsoleCommand( "report", nick, "Just awarded an item: " .. reason:GetValue() , true )
		end
		commandList:AddItem( award )

		local innode = vgui.Create( "DButton" )
		innode:SetText("In Node")
		innode.DoClick = function() RunConsoleCommand( "admin", "innode" ) end
		commandList:AddItem( innode )

		local outnode = vgui.Create( "DButton" )
		outnode:SetText("Out Node")
		outnode.DoClick = function() RunConsoleCommand( "admin", "outnode" ) end
		commandList:AddItem( outnode )
	end
end