// Copyright 2011 TheBamfords <- lawl
include("json.lua")

function loadDonators()
	local contents = file.Read("mafiarp/donators.txt")
	if contents ~= nil then
		if string.len(contents) > 1 then
			donators = Json.Decode( contents )
		end
	end
end
concommand.Add("loaddonators", loadDonators)

function saveDonator( ply, command, args )
	if ply:IsSuperAdmin() then
		local target = FindPlayer( args[1] )
		donators[target:SteamID()] = 1
		local contents = Json.Encode( donators )
		file.Write("mafiarp/donators.txt", contents)
	end
end
concommand.Add("savedonator", saveDonator)

function removeDonator( ply, command, args )
	if ply:IsSuperAdmin() then
		local target = FindPlayer( args[1] )
		donators[target:SteamID()] = nil
		local contents = Json.Encode( donators )
		file.Write("mafiarp/donators.txt", contents)
	end
end
concommand.Add("removedonator", removeDonator)

// WHEN THE GAMEMODE STARTS GET EVERYTHING FROM THE FILE AND PUT IT IN A TABLE
// WHEN A PERSON REQUESTS A LOAD DO THE SAME THING
// WHEN A SAVE IS MENT TO HAPPEN, UPDATE THE FILE AND LOAD IT AGAIN