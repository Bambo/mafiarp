// Copyright 2012
hook.Add("PlayerDeath", "DeathGhosting", function(victim, inflictor, killer)
	victim.deathpos = victim:GetPos()
	timer.Simple(120, function()
		if not IsValid(victim) then return end -- If they disconnect
		victim.deathpos = nil
		victim:ChatPrint("You are no longer under NLR restrictions")
	end)
end)

hook.Add("SetupMove", "MoveGhosting", function(ply, move)
	if ply.deathpos and ply.deathpos:Distance(ply:GetPos()) < 500 then 
		--local ang = move:GetMoveAngles():Forward() why did you do this xD
		local ang = ply:GetVelocity()	-- just get his velocity and negate it!
		-- there are still problems with this, i think people can still glitch through via prop-pushing

		move:SetVelocity(-ang*2)	-- 700 was abit too vicious xD and i don't think the BOUNCE effect 
	end
end)
