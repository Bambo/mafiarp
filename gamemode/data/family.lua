// Copyright 2011 TheBamfords <- lawl

function Stopper( ply, id )
	timer.Create("Stopper-" .. ply:SteamID() .. "" .. id, 5, 1, function()
		ClearStopper( ply, id )
	end)
end

function ClearStopper( ply, id )
	timer.Destroy("Stopper-" .. ply:SteamID() .. "" .. id)
end

function GetStopper( ply, id )
	return timer.IsTimer("Stopper-" .. ply:SteamID() .. "" .. id)
end

function FamilyGiveExp( amount, family )
	if 10 < tonumber( familys[family].level ) then
		-- LOL I TROLE YOU USE MAH CRAZY IF STATEMENT
	else
		familys[family].exp = familys[family].exp + amount
		if familys[family].exp >= GetMaxFamilyExp( family ) then
			familys[family].level = familys[family].level + 1
			familys[family].exp = 1
		end
	end
end

function IsFamily( family )
	if familys[family] == nil then
		return false
	else
		return true
	end
end

function IsInFamily( ply, family )
	if players_table[ply:SteamID()].family == family then
		return true
	else
		return false
	end
end

function HasFamily( ply )
	if players_table[ply:SteamID()].family ~= "" then
		return true
	else
		return false
	end
end

function GetFamily( ply )
	return players_table[ply:SteamID()].family
end

function GetFamilyLevel( ply )
	local family = players_table[ply:SteamID()].family
	return familys[family].level
end


function GetMaxFamilyExp( name )
	local level = familys[name].level
	if level ~= nil then
		return tonumber((level+1000) * math.pow(2, 11/3))
	elseif level == nil then
		return 1
	end
end

function SaveFamily( name )
	local sqls = sql.Query("UPDATE familys SET exp = " .. familys[name].exp .. ", level = " .. familys[name].level ..", members = " .. familys[name].members .. " WHERE name = '" .. sql.SQLStr(name) .. "'")
end

function LoadFamily( name )
	local exp = sql.QueryValue("SELECT exp FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")
	local level = sql.QueryValue("SELECT level FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")
	local owner = sql.QueryValue("SELECT owner FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")
	local members = sql.QueryValue("SELECT members FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")
	if owner == nil then
		players_table[ply:SteamID()].family = ""
		players_table[ply:SteamID()].owner = false
	else
		familys[name] = { exp = exp, level = level, owner = owner, members = members }
	end
end

function CreateFamily( ply, command, args )
	local name = args[1]
	if players_table[ply:SteamID()].family == "" then
		if not GetStopper( ply, 1 ) then
			local check = sql.QueryValue("SELECT level FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")
			if check ~= nil then
				Notify(ply, 3, 3, "That family already exists!")
			else
				sql.QueryValue("INSERT INTO familys (name, owner, exp, level, members) VALUES ('" .. sql.SQLStr(name) .. "', '" .. ply:SteamID() .. "', 1, 1, 1 )")
				if not ply:IsAdmin() then
					ply:SetNWInt("money", ply:GetNWInt("money") - 250000)
				end
				familys[name] = {exp = 1, level = 1, owner = ply:SteamID(), members = 1}
				GlobalSave()
				Notify(ply, 3, 3, "Family created ... " .. name .. "!")
				players_table[ply:SteamID()].family = name
				players_table[ply:SteamID()].owner = true
				SendCreds( ply )
				sql.Query("UPDATE player_info SET family = " .. sql.SQLStr(name) .. " WHERE unique_id = '" .. ply:SteamID() .. "'")
			end
			Stopper( ply, 1 )
		else
			Notify(ply, 3, 3, "Sorry you have to wait at max 5 seconds!")
		end
	else
		Notify(ply, 3, 3, "Sorry you are already in a family!")
	end
end
concommand.Add("createfamily", CreateFamily)

function SendCreds( ply )
	umsg.Start("SendCreds", ply)
		umsg.String(players_table[ply:SteamID()].family)
		umsg.Bool(players_table[ply:SteamID()].owner)
	umsg.End()
end

function DeleteFamily( ply )
	local name = players_table[ply:SteamID()].family
	if name ~= "" then
		if not GetStopper( ply, 2 ) then	-- make sure we're not spamming
			local check = sql.QueryValue("SELECT level FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")	-- check if the family actually exists
			if check ~= nil then
				local owner = sql.QueryValue("SELECT owner FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")	-- check if your the owner
				if owner == ply:SteamID() then
					sql.QueryValue("DELETE FROM familys WHERE name='" .. sql.SQLStr(name) .. "'")	-- remove it from the familys table
					familys[name] = nil	-- delete it from the  temp storage
					Notify(ply, 3, 3, "Deleted family " .. name .. "!")
					GlobalSave()	-- update the familys sustained table
					players_table[ply:SteamID()].family = ""	-- make the owners family ""
					players_table[ply:SteamID()].owner = false -- make the owners owner flag false
					sql.QueryValue("UPDATE player_info SET family = '' WHERE family='" .. name .. "'")
					sql.Query("UPDATE player_info SET family = '' WHERE unique_id = '" .. ply:SteamID() .. "'")	-- update the players family status
					SendCreds( ply )

					for _,v in pairs(player.GetAll()) do
						if players_table[v:SteamID()].family == name then
							players_table[v:SteamID()].family = ""
							players_table[v:SteamID()].owner = false
						end
					end
					
				else
					Notify(ply, 3, 3, "You do not own this family!")
				end
			else
				Notify(ply, 3, 3, "Family " .. name .. " does not exist!")
			end
	
			Stopper( ply, 2 )
		else
			Notify(ply, 3, 3, "Sorry you have to wait at max 5 seconds!")
		end
	else
		Notify(ply, 3, 3, "Sorry you are not in a family!")
	end
end
concommand.Add("deletefamily", DeleteFamily)


function printeveryone()
	if ply:IsAdmin() then
		PrintTable(players_table)
	end
end
concommand.Add("printppl", printeveryone)

function InvitePlayer( ply, command, args )
	if not GetStopper( ply, 3 ) then
		local owner = sql.QueryValue("SELECT owner FROM familys WHERE name='" .. sql.SQLStr(players_table[ply:SteamID()].family) .. "'")
		local members = tonumber(sql.QueryValue("SELECT members FROM familys WHERE name='" .. sql.SQLStr(players_table[ply:SteamID()].family) .. "'"))
		if members <= 4 then
			if owner == ply:SteamID() then
				local v = Player(tonumber(args[1]))
				umsg.Start("SendInvite", v)
					umsg.String(players_table[ply:SteamID()].family)
				umsg.End()
				table.insert(invites, {players_table[ply:SteamID()].family, v:SteamID()})
				Notify(ply, 3, 3, "Invitiation sent!")
			else
				Notify(ply, 3, 3, "You do not own the family you belong to!")
			end
			Stopper( ply, 3 )
		else
			Notify(ply, 3, 3, "Sorry your family is full!")
		end
	else
		Notify(ply, 3, 3, "Sorry you have to wait at max 5 seconds!")
	end
end
concommand.Add("inviteplayer", InvitePlayer)

function KickPlayer( ply, command, args )
	// kickplayer NAME
	if not GetStopper( ply, 3 ) then
		local owner = sql.QueryValue("SELECT owner FROM familys WHERE name='" .. sql.SQLStr(players_table[ply:SteamID()].family) .. "'")
		if owner == ply:SteamID() then
			local target = FindPlayer( args[1] )
			local fam = players_table[ply:SteamID()].family
			local members = sql.QueryValue("SELECT members FROM familys WHERE name='" .. sql.SQLStr(fam) .. "'")
			sql.Query("UPDATE familys SET members = '" .. members - 1 .."' WHERE name='" .. sql.SQLStr(fam) .. "'")

			familys[fam].members = members - 1
			players_table[target:SteamID()].family = ""
			send_data( target )
			Notify(target, 3, 3, "You have been kicked out of your family!")
		end
	else
		Notify(target, 3, 3, "Sorry you have to wait at max 5 seconds!")
	end
end
concommand.Add("kickplayer", KickPlayer)

function AcceptInv( ply, command, args )
	for k,f in ipairs(invites) do
		if args[1] == f[1] and ply:SteamID() == f[2] then
			players_table[ply:SteamID()].family = args[1]
			local members = sql.QueryValue("SELECT members FROM familys WHERE name='" .. sql.SQLStr(players_table[ply:SteamID()].family) .. "'")
			familys[players_table[ply:SteamID()].family].members = members + 1
			sql.Query("UPDATE familys SET members = '" .. members + 1 .."' WHERE unique_id = '" .. ply:SteamID() .. "'")
			Notify(ply, 3, 3, "You are now part of the family: " .. args[1])
			send_data( ply )
			table.remove(invites, k)
		end
	end
end
concommand.Add("acceptinv", AcceptInv)

function Leavefamily( ply, command, args )
	local members = sql.QueryValue("SELECT members FROM familys WHERE name='" .. sql.SQLStr(players_table[ply:SteamID()].family) .. "'")
	sql.Query("UPDATE familys SET members = '" .. members - 1 .."' WHERE unique_id = '" .. ply:SteamID() .. "'")

	familys[players_table[ply:SteamID()].family].members = members - 1
	players_table[ply:SteamID()].family = ""
	send_data( ply )
	Notify(ply, 3, 3, "You left your family!")
end
concommand.Add("leavefamily", Leavefamily)
