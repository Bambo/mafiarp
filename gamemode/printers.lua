-- i made it global so we can use it in the base.lua
printers = {
	{ name = "Money Printer", model = "models/props_c17/consolebox01a.mdl", defMoney = 20, defExp = 50, useLevel = 1, entName = "money_printer" },
	{ name = "Money Printer Diamond", model = "models/props_junk/Shoe001a.mdl", defMoney = 10000, defExp = 2000, useLevel = 40, entName = "money_printer_diamond"},
	{ name = "Money Printer Gold", model = "models/props_wasteland/laundry_washer003.mdl", defMoney = 1000, defExp = 500, useLevel = 20, entName = "money_printer_gold"},
	{ name = "Money Printer Platinum", model = "models/props_lab/servers.mdl", defMoney = 2000, defExp = 800, useLevel = 30, entName = "money_printer_platinum" },
	{ name = "Money Printer Silver", model = "models/props_lab/partsbin01.mdl", defMoney = 800, defExp = 300, useLevel = 1, entName = "money_printer_silver"}
}

local ENT = {}
ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.Author = "Bambo & Macendo"
ENT.Spawnable = false
ENT.AdminSpawnable = false
if SERVER then
function ENT:Initialize()
	-- print(self.model) y u do dis?
	self:SetModel(self.model or "models/props_c17/consolebox01a.mdl")
	self.model = nil
	self:SetColor(255,0,0,255)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()
	if phys:IsValid() then phys:Wake() end
	phys:SetMass( 200 )	-- make it not weight the mass of the death star...

	self.flamesChance = 22

	self.damage = 100
	self.IsMoneyPrinter = true

	--self.default_money = 20
	--self.default_exp = 50

	self.Active = true

	--self.printAmountMoney = 20
	--self.printAmountExp = 50

	self.storedMoney = 0
	self.storedExp = 0

	timer.Create("CreateMoneyBag"..self:EntIndex(), 60, 0, self.CreateMoneybag, self )
	timer.Create("CheckSphere" .. self:EntIndex(), 2, 0, self.checkSphere, self )
end

function ENT:checkSphere()
	if not self.Active then return end
	timer.Adjust("CreateMoneyBag" .. self:EntIndex(), 60, 0, self.CreateMoneybag, self )

	self.printAmountMoney = self.default_money

	for k, v in pairs( ents.FindInSphere( self:GetPos(), 200 ) ) do
		if v:GetClass() == "printer_booster" then
			self.printAmountMoney = self.default_money + (self.default_money / 2)
		elseif v:GetClass() == "printer_cooler" then
			self.flamesChance = 1
		elseif v:GetClass() == "printer_rateinc" then
			timer.Adjust("CreateMoneyBag" .. self:EntIndex(), 30, 0, self.CreateMoneybag, self )
		end
	end

end

function ENT:OnTakeDamage(dmg)
	if not self.Active then return end
	if self.burningup then return end

	self.damage = self.damage - dmg:GetDamage()
	if self.damage <= 0 then
		local rnd = math.random(1, 10)
		if rnd < 3 then
			self:BurstIntoFlames()
		else
			self:KillNow()
		end
	end
end

function ENT:KillNow()
	if not self.Active then return end
	self:Destruct()
	self:Remove()

	timer.Destroy("CreateMoneyBag" .. self:EntIndex() )
	timer.Destroy("CheckSphere" .. self:EntIndex() )
end

function ENT:Destruct()
	if not self.Active then return end
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart(vPoint)
	effectdata:SetOrigin(vPoint)
	effectdata:SetScale(1)
	util.Effect("Explosion", effectdata)
	Notify(self:GetNWEntity("owning_ent") , 1, 4, "Your money printer has exploded!")

	timer.Destroy("CreateMoneyBag" .. self:EntIndex() )
	timer.Destroy("CheckSphere" .. self:EntIndex() )
end

function ENT:BurstIntoFlames()
	if not self.Active then return end
	Notify(self:GetNWEntity("owning_ent"), 1, 4, "Your money printer is overheating!")
	self.burningup = true
	local burntime = math.random(8, 18)
	self:Ignite(burntime, 0)
	timer.Simple(burntime, self.Fireball, self)
end

function ENT:Fireball()
	if not self.Active then return end
	local dist = math.random(20, 280) -- Explosion radius
	self:Destruct()
	for k, v in pairs(ents.FindInSphere(self:GetPos(), dist)) do
		if not v:IsPlayer() and not v.IsMoneyPrinter then v:Ignite(math.random(5, 22), 0) end
	end
	self:Remove()
end

function ENT:Use(activator, caller)
	if not self.Active then return end
	if activator:IsPlayer() then
		local total = GetPrinterNum( activator )	-- look in base.lua, n00b
		if total <= 10 then
			if self.storedMoney > 0 then
				if tonumber(GetLevel( activator )) >= self.useLevel then

					if activator ~= self:GetNWEntity("owning_ent") or self:GetNWEntity("owning_ent") == nil then
						self:SetNWEntity("owning_ent", activator)
					end

					AddMoneyz( self.storedMoney, activator )
					AddExp( self.storedExp, activator )

					Notify(activator, 2, 3, "You collected: $" .. self.storedMoney .. ", and " .. self.storedExp .. " exp!")
					self.storedMoney = 0
					self.storedExp = 0
					self:SendData()

				else
					Notify(activator, 2, 3, "You are not a high enough level")
				end
			end
		else
			self:KillNow()
			Notify(activator, 2, 3, "You already have 10 printers! derpface." )	-- lulwat
		end
	end
end

function ENT:CreateMoneybag()
	if not self.Active then return end
	if not ValidEntity(self) then return end
	if self:IsOnFire() then return end
	if math.random(1, self.flamesChance) == 3 then self:BurstIntoFlames() end

	self.storedMoney = self.storedMoney + self.printAmountMoney
	self.storedExp = self.storedExp + self.printAmountExp
	self:SendData()
end

function ENT:OnRemove()
	if not self.Active then return end
	timer.Destroy("CreateMoneyBag" .. self:EntIndex() )
	timer.Destroy("CheckSphere" .. self:EntIndex() )
end

function ENT:SendData()
	if not self.Active then return end
	if not ValidEntity( self ) then return end
	umsg.Start( "MPrintUp"  )
		umsg.Entity(self)
		umsg.Long( self.storedMoney )
	umsg.End()
end

function ENT:Active( var )
	self.Active = var
end

elseif CLIENT then
	function ENT:Initialize()
		self.money = 0
	end

	function ENT:Draw()
		self.Entity:DrawModel()
	end
	-- Takes care of all dem printers

	usermessage.Hook("MPrintUp", function(data)
		local ent = data:ReadEntity()
		ent.money = data:ReadLong()
		if not IsValid(ent) then return end
		if ent.money and ent.money > 0 then
			ent:SetColor( 0, 255, 0, 255 )
		else
			ent:SetColor( 255, 0, 0, 255 )
		end
	end)
end
for k,v in pairs(printers) do
	local ENT = table.Copy(ENT)
	ENT.PrintName = v.name
	ENT.default_money = v.defMoney
	ENT.default_exp = v.defExp
	ENT.printAmountMoney = v.defMoney
	ENT.printAmountExp = v.defExp
	ENT.useLevel = v.useLevel
	ENT.model = v.model
	ENT.untouchable = true
	scripted_ents.Register(ENT, v.entName, false)
end
