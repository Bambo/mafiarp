AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self.Entity:SetModel("models/Items/combine_rifle_ammo01.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()

	if phys and phys:IsValid() then phys:Wake() end
	self.Set = false
end

function ENT:Use(activator, caller)
	if tonumber(GetLevel(activator)) >= 30 then
		if not self.Set then
			timer.Simple( 2, self.Explode, self )
			Notify(activator, 2, 3, "2 Seconds untill explosion...")
			self.Set = true
			self.Owner = activator
		end
	else
		Notify(activator, 2, 3, "Sorry but you need to be level 30+")
	end
end

function ENT:Explode()
	local explo = ents.Create( "env_explosion" )
	explo:SetOwner( self.Owner )
	explo:SetPos( self:GetPos() )
	explo:SetKeyValue( "iMagnitude", "0" )
	explo:Spawn()
	explo:Activate()
	explo:Fire( "Explode", "", 0 )
		
	local shake = ents.Create( "env_shake" )
	shake:SetOwner( self.Owner )
	shake:SetPos( self:GetPos() )
	shake:SetKeyValue( "amplitude", "2000" )
	shake:SetKeyValue( "radius", "900" )
	shake:SetKeyValue( "duration", "2.5" )
	shake:SetKeyValue( "frequency", "255" )
	shake:SetKeyValue( "spawnflags", "4" )
	shake:Spawn()
	shake:Activate()
	shake:Fire( "StartShake", "", 0 )
	
	local dmginfo = DamageInfo()
	dmginfo:SetDamage( 250 )
	dmginfo:SetDamageType( DMG_BLAST )
	dmginfo:SetAttacker( self.Owner )
		
	for _, ent in pairs( ents.FindInSphere( self:GetPos(), math.random( 200, 300 ) ) ) do
		if ent:IsPlayer() then
			dmginfo:SetDamageForce( ( ent:GetPos() - self:GetPos() ) * 500 )
			ent:TakeDamageInfo( dmginfo )
		end
	end
		
	for _, ent in ipairs( ents.FindInSphere( self:GetPos(), 30 ) ) do
		if ent:GetClass() == "prop_physics" then
			local rand = math.random(1, 100)
			if rand > 20 then
				ent:Remove()
			end
		end
	end

	log( self.Owner, "Just set off a Big Daddie" )
	
	self:Remove()
end