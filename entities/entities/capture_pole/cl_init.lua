include("shared.lua")

function ENT:Initialize()
        self.Name = "Unowned"
        self.Capping = false
	self.StepValue = 8 -- Size of borders, which means this is the minimum size it doesn't fuck up
end

usermessage.Hook("SendOwner", function(um)
	local ent = um:ReadEntity()
	ent.Name = um:ReadString()
	-- If we changed owner, we ain't capping
	ent.Capping = false
end)
usermessage.Hook("Capping", function(um)
	um:ReadEntity().Capping = true
end)

-- One function to rule em all 
usermessage.Hook("Step", function(data)
	data:ReadEntity().StepValue = data:ReadLong()
end)

function ENT:Draw()
        self.Entity:DrawModel()
end

function ENT:GetName()
	return self.Name
end

function ENT:GetStep()
        return self.StepValue
end

function ENT:IsCapping()
        return self.Capping
end
