AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.StepValue = 0
	self.Activator = nil
	self.Cap = 1
	self:SetModel("models/props_trainstation/trainstation_post001.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()
	if phys:IsValid() then phys:Wake() end
    
	self.Family = "Unowned"
	timer.Create("GiveFamilyExp" .. self:EntIndex(), 5, 0, self.GiveExp, self)
	timer.Create("ClearFamily" .. self:EntIndex(), 3600, 0, self.Clear, self )
end

function ENT:Use(activator, caller)
	if activator:IsPlayer() and HasFamily( activator ) then
		if self.Family ~= GetFamily( activator ) then -- do not own the pole
			if self.Cap == 1 then umsg.Start("Capping") umsg.Entity(self) umsg.End() end

			self.Cap = self.Cap or 0

			-- Check if pole is captured (at 100), if not continue capturing
			if self.Cap == 100 then
				self.Cap = 0
				self.Family = GetFamily(activator)
				umsg.Start("SendOwner")
					umsg.Entity(self)
					umsg.String(self.Family)
				umsg.End()
				-- Reset timer
				timer.Start("ClearFamily"..self:EntIndex())
			else
				self.Cap = self.Cap + 1

				if self.Cap % 10 == 0 then
					umsg.Start("Step")
						umsg.Entity(self)
						umsg.Long(self.Cap)
					umsg.End()
				end
			end

			-- Throttle attempt message
			self.lastTime = self.lastTime or CurTime() - 10
			if CurTime() - 5 > self.lastTime then
				self.lastTime = CurTime()
				Notify(activator, 2, 3, "You have attempted to capture this pole!")
			end
		end	
	end
end

function ENT:Clear()
	self.Family = "Unowned"
	umsg.Start("SendOwner")
		umsg.Entity(self)
		umsg.String("Unowned")
	umsg.End()
end

function ENT:GiveExp()
	if IsFamily( self.Family ) then
		FamilyGiveExp( GetMaxFamilyExp( self.Family )/50, self.Family)
	end
end
