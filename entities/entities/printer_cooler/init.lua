-- RRPX Money Printer reworked for DarkRP by philxyz
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/weapons/w_c4_planted.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()
	if phys:IsValid() then phys:Wake() end
	self.ID = math.random(1, 100000)
	self.health = 1
	self.active = false
	
	timer.Create(self.ID, 300, 0, self.CheckExplode, self)
end

function ENT.GravgunPickupAllowed() return true end

function ENT:CheckExplode()
	if math.random(1, 300) <= 50 then
		self:Destruct()
		self:Remove()
	end
end

function ENT:OnTakeDamage(dmg)
	self.health = self.health - dmg:GetDamage()
	if self.health <= 0 then
		self:Destruct()
		self:Remove()
	end
end

function ENT:Destruct()
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart(vPoint)
	effectdata:SetOrigin(vPoint)
	effectdata:SetScale(1)
	util.Effect("Explosion", effectdata)
end

function ENT:OnRemove()
	Notify(self:GetNWEntity("owning_ent"), 1, 4, "Your printer cooler has exploded!")
	timer.Destroy( self.ID )
end

function ENT:Think()
end
