AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self.Entity:SetModel("models/props_c17/pottery_large01a.mdl")
	self.Entity:SetMaterial("models/shadertest/shader4")
    self:PhysicsInit(SOLID_VPHYSICS)
    self:SetMoveType(MOVETYPE_NONE)
    self:SetSolid(SOLID_VPHYSICS)
    local phys = self:GetPhysicsObject()
    if phys:IsValid() then phys:Wake() end
end

function ENT:Use( activator, caller )
	if not GetStopper( activator, 10 ) then
		for k,v in pairs( ents.FindByClass("event_node_out") ) do
			local outpos = v:GetPos()
			local x = outpos.x + 50
		 
			activator:SetPos( Vector( x, outpos.y, outpos.z ) )
			Stopper( activator, 10 )
			activator:SetNoCollideWithTeammates( true )
			timer.Simple( 5, self.CollideWithTeammates, self, activator )
		end
	end
end

function ENT:CollideWithTeammates( ply )
	ClearStopper( ply, 10 )
	ply:SetNoCollideWithTeammates( false )
end