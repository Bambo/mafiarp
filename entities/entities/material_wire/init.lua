AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self.Entity:SetModel("models/props_c17/TrapPropeller_Lever.mdl")
	self.Entity:SetMaterial("models/debug/debugwhite")
	self.Entity:SetColor( 199, 3, 30, 255 )
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	local phys = self.Entity:GetPhysicsObject()
	phys:SetMass( 200 )
	if phys and phys:IsValid() then phys:Wake() end
end