include("shared.lua")

function ENT:Initialize()
	self.id = math.random(1, 10000)
	self.pos = {}
	timer.Create(self.id, 1, 0, self.UpdatePos, self)
end

function ENT:Draw()
	self.Entity:DrawModel()
	local Laser = Material( "cable/hydra" )
 	if self.pos ~= nil then
		for k, v in ipairs(self.pos) do
			render.SetMaterial( Laser )
			render.DrawBeam( self:GetPos(), v, 5, 1, 1, Color( 255, 255, 255, 255 ) ) 
		end
	end
end

function ENT:UpdatePos()
	self.pos = {}
	for k, v in pairs( ents.FindInSphere( self:GetPos(), 200 ) ) do
		if string.find(v:GetClass(), "money_") ~= nil then
			table.insert(self.pos, v:GetPos() )
        end
	end
end

function ENT:OnRemove()
	timer.Destroy( self.id )
	self.pos = nil
end

function ENT:Think()
end