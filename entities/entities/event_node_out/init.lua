AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self.Entity:SetModel("models/props_c17/pottery_large01a.mdl")
	self.Entity:SetMaterial("models/shadertest/shader4")
    self:PhysicsInit(SOLID_VPHYSICS)
    self:SetMoveType(MOVETYPE_NONE)
    self:SetSolid(SOLID_VPHYSICS)
    local phys = self:GetPhysicsObject()
    if phys:IsValid() then phys:Wake() end
end

function ENT:Use( activator, caller )
	for k,v in pairs( ents.FindByClass("event_node_in") ) do
		local inpos = v:GetPos()
		local x = inpos.x + 50
	 
		activator:SetPos( Vector( x, inpos.y, inpos.z ) )
		activator:SetNoCollideWithTeammates( true )
		timer.Simple( 5, self.CollideWithTeammates, self, activator )
	end
end

function ENT:CollideWithTeammates( ply )
	ply:SetNoCollideWithTeammates( false )
end