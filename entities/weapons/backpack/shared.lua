if SERVER then
	AddCSLuaFile("shared.lua")
end

if CLIENT then
	SWEP.PrintName = "Backpack"
	SWEP.Slot = 1
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Author = "Backpack"
SWEP.Instructions = "Left click to pick up and reload to open"
SWEP.Contact = ""
SWEP.Purpose = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix	 = "rpg"

SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

if CLIENT then
	SWEP.FrameVisible = false
end

function SWEP:Initialize()
	if SERVER then self:SetWeaponHoldType("normal") end
end

function SWEP:Deploy()
	if SERVER then
		self.Owner:DrawViewModel(false)
		self.Owner:DrawWorldModel(false)
	end
end

local blacklist = {"drug_lab", "money_printer", "meteor", "microwave", "door", "func_", "player", "beam", "worldspawn", "env_", "path_", "spawned_shipment", "prop_physics"}

function SWEP:PrimaryAttack()
	if CLIENT then return end

	self.Weapon:SetNextPrimaryFire(CurTime() + 0.2)
	local trace = self.Owner:GetEyeTrace()

	if not ValidEntity(trace.Entity) then
		return
	end
	
	if self.Owner:EyePos():Distance(trace.HitPos) > 65 then
		return
	end
	
	local phys = trace.Entity:GetPhysicsObject()
	if not phys:IsValid() then return end
	local mass = phys:GetMass()
	
	if SERVER then
		self:SetWeaponHoldType("pistol")
		timer.Simple(0.2, function(wep) if wep:IsValid() then wep:SetWeaponHoldType("normal") end end, self)
	end
	
	for k,v in pairs(blacklist) do 
		if string.find(string.lower(trace.Entity:GetClass()), v) then
			Notify(self.Owner, 1, 4, "You can not put this in your backpack!")
			return
		end
	end

	if invGetSize( self.Owner ) then	-- check if backpack is full
		trace.Entity:SetNoDraw(true)
		trace.Entity:SetCollisionGroup(0)
		local phys = trace.Entity:GetPhysicsObject()
		phys:EnableMotion(true)
		trace.Entity:SetMoveType(MOVETYPE_VPHYSICS)
		if phys:IsValid() then
			phys:EnableCollisions(false)
			phys:Wake()
		end
		if trace.Entity:GetClass() == "spawned_weapon" then
			-- weapon class just comes as spawned_weapon if you don't get its weaponclass
			invAdd( self.Owner, trace.Entity.weaponclass, trace.Entity:GetModel() )
		else
			invAdd( self.Owner, trace.Entity:GetClass(), trace.Entity:GetModel() )
		end
	else
		Notify(self.Owner, 1, 4, "Your backpack is full!")
	end
	
end

SWEP.OnceReload = false
function SWEP:Reload()
	if CLIENT or self.Weapon.OnceReload then return end
	self.Weapon.OnceReload = true
	timer.Simple(0.5, function() self.Weapon.OnceReload = false end)
	
	umsg.Start("OpenInventory", self.Owner)
	umsg.End()
end